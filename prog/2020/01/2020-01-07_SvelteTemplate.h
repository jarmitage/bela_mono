#include <stdio.h>
#include <vector>
#include <string>

#include <Bela.h>

class Program {
private:
	std::string programName = "2020-01-07_SvelteTemplate";
	Gui   *gui;
	Scope *scope;

public:
	bool setup (BelaContext *context, void *userData, Gui &_gui, Scope &_scope) {
		// Gui
	    gui = &_gui;
	    gui->setup(context->projectName);
	    gui->setControlDataCallback([this](JSONObject& root, void* customData)->bool {
	      onControl(root);
	      return true;
	    });

        // Scope
	    scope = &_scope;
	    scope->setup(4, context->audioSampleRate);

	    printProgramName();
	    return true;
	}
	void render (BelaContext *context, void *userData){
		for (int n = 0; n < context->audioFrames; ++n) {
		}
	}
	void onControl(JSONObject root) {
		std::wstring cmd;
		rt_printf("Command received\n");
		sendToGui();
		// if (json_u.isCmd(root, cmd)) {
		// 	JSONValue *args = root[L"args"];
		// 		 if (json_u.isWS(cmd, L"updateIOGain"))        json_p.onUpdateIOGain(io_gain, args);
		// 	else if (json_u.isWS(cmd, L"updateResPitch"))      json_p.onUpdateResPitch(res, args);
		// }
	}
	void sendToGui(){
		JSONObject root;
		root[L"note"] = new JSONValue (0);
		JSONValue* value = new JSONValue (root);
		gui->sendControl(value);
		rt_printf("send value: %d\n", 0);
	}
	void cleanup (BelaContext *context, void *userData){}
	void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
	Program(){}
	~Program(){}
};
