#include <stdio.h>
#include <string>
#include <Bela.h>

class Program
{
private:
  std::string programName = "Program";
public:
  bool setup (BelaContext *context, void *userData){
  	printProgramName();
  	return true;
  }
  void render (BelaContext *context, void *userData){

  }
  void cleanup (BelaContext *context, void *userData){}
  void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
  Program(){}
  ~Program(){}
};
