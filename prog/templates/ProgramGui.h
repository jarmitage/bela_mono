#include <stdio.h>
#include <string>
#include <Bela.h>
#include "libraries/Gui/Gui.h"

class Program
{
private:
  std::string programName = "ProgramGui";
  Gui gui;
public:
  bool setup (BelaContext *context, void *userData){
    gui.setup(context->projectName);
  	printProgramName();
  	return true;
  }
  void render (BelaContext *context, void *userData){

  }
  void cleanup (BelaContext *context, void *userData){}
  void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
  Program(){}
  ~Program(){}
};
