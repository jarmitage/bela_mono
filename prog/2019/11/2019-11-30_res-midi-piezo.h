#include <vector>
#include <string>
#include <map>
#include <algorithm>

// Bela deps
#include <Bela.h>
#include <libraries/Midi/Midi.h>

// Lib
#include "lib/Filters/Resonators/Resonators.h"
#include "lib/Devices/Sampler/SampleTriggerDigital.h"
#include "lib/Utils/Pitch.h"

// set to true if using away from web app
bool loadDefaultModels = true;
bool midiTriggerMode   = true;
float input_gain  = 0.1; // MIDI: 0.01
float output_gain = 10.0; // MIDI: 10.0
const char* midiPort = "hw:1,0,0";

// Objects
Resonators res;
SampleTriggerDigital samplers[4];
Midi midi;
Pitch p;

// Resonator pitches & models, MIDI samples and trigger notes
std::vector<std::string> resPitches   = {"c3", "g3", "d4", "a4"};
std::vector<std::string> resModels    = {"handdrum","handdrum","handdrum","handdrum"};
std::vector<std::string> samples      = {"snare", "snare", "snare", "snare"};
std::vector<std::string> midiTriggers = {"c4", "d4", "e4", "f4"};
std::vector<bool>        midiNoteOns  = {false, false, false, false};
int currentNoteIndex = 0;

std::vector<std::string> robbieModels = {"2019-11-28_130642_metallic","2019-11-28_130708_metallic","2019-11-28_130739_metallic","2019-11-28_130919_metallic"};
std::string robbiePath = "data/models/pilot/sessions/robbie/";

int findIndexOfStringInVector(std::string str, std::vector<std::string> vec) {
  // TODO: turn into template function (generic findIndex)
  std::vector<std::string>::iterator it = std::find(vec.begin(), vec.end(), str);
  if (it != vec.cend())
    return std::distance(vec.begin(), it);
  else
    return -1;
}

void midiTriggerSampler(MidiChannelMessage msg) {
  if(msg.getType() == kmmNoteOn) {
    std::string noteOnName = p.midiToNoteName(msg.getDataByte(0));
    int velocity = msg.getDataByte(1);
    // rt_printf("[render] midiTriggerSampler() note %d %s velocity %d\n", noteOn, noteName.c_str() , velocity);
    int noteIndex = findIndexOfStringInVector(noteOnName, midiTriggers);
    if (noteIndex > -1) samplers[noteIndex].trigger(velocity);
  }
}

void midiTriggerPiezo(MidiChannelMessage msg) {
  
  int noteIndex = findIndexOfStringInVector(p.midiToNoteName(msg.getDataByte(0)), midiTriggers);

  if (noteIndex > -1) {
    if(msg.getType() == kmmNoteOn)
      currentNoteIndex = noteIndex;
      midiNoteOns[noteIndex] = true;
    if(msg.getType() == kmmNoteOff)
      midiNoteOns[noteIndex] = false;
  }

  rt_printf("[render] midiTriggerPiezo() noteOns:");
  for (int i = 0; i < midiNoteOns.size(); ++i)
    rt_printf("%s ", midiNoteOns[i] ? "true" : "false");
  rt_printf("\n");

}

void midiMessageCallback(MidiChannelMessage msg, void* arg){
  // msg.prettyPrint();
  // midiTriggerPiezo(msg);
  midiTriggerSampler(msg);

}

void setupMIDI(const char* port){
  midi.readFrom(port);
  midi.writeTo(port);
  midi.enableParser(true);
  midi.setParserCallback(midiMessageCallback, (void*) port);
  rt_printf("[render] setupMIDI() setup on port %s\n", port);
}

bool setup (BelaContext *context, void *userData) {
  setupMIDI(midiPort);

  res.setup(resPitches.size(), context->projectName, context->audioSampleRate, context->audioFrames);

  if (loadDefaultModels) {
    for (int i = 0; i < resPitches.size(); ++i) {
      // res.load(i, "data/models/pilot/"+resModels[i]+".json");
      // res.shiftToNote(i, resPitches[i]);

      // robbie comparison models
      res.load(i, robbiePath+robbieModels[i]+".json");
      res.shiftToNote(i, resPitches[0]);
      
      res.setRes(i, i); // set 
      samplers[i].setup("data/samples/"+samples[i]+".wav");
    }

    res.updateAll();
  }
  return true;
}

void render (BelaContext *context, void *userData) { 
  for (unsigned int n = 0; n < context->audioFrames; ++n) {
    // v1 piezo
    // float in = audioRead(context, n, 0);
    // float out = 0.0;
    // for (int i = 0; i < resPitches.size(); ++i) {
    //   if (midiNoteOns[i] == true) {
    //     out += res.render(i, in * input_gain);
    //     i = resPitches.size();
    //   }
    // }

    // v2 piezo
    // float out = res.render(currentNoteIndex, audioRead(context, n, 0) * input_gain);

    // v3 sampler
    float out = 0.0;
    for (int i = 0; i < resPitches.size(); ++i)
      out += res.render(i, samplers[i].render(0) * input_gain);

    audioWrite(context, n, 0, out * output_gain);
    audioWrite(context, n, 1, out * output_gain);
  }
}

void cleanup (BelaContext *context, void *userData){}
