#include <vector>
#include <Bela.h>
#include "lib/Filters/Resonators/Resonators.h"

Resonators res;
std::string pitch = "c3";
int banks = 2; // TODO: set this from web app?

bool loadDefaultModels = false; // set to true if using away from web app

// TODO: Naive compression
float input_gain = 0.25;
float output_gain = 5;

bool setup (BelaContext *context, void *userData) {

  res.setup(banks, context->projectName, context->audioSampleRate, context->audioFrames);
  res.setNote(pitch); // set global note

  // TODO: turn into array of model names and pitches
  if (loadDefaultModels) {

    rt_printf("[render.cpp] Loading default models\n");

    int bank = 0;
    res.load(bank, "models/handdrum.json"); // load model into slot 0
    res.shiftToNote(bank, pitch);           // shift model 0 to pitch
    res.setRes(bank, bank);                 // set bank 0 to model 0

    bank = 1;
    res.load(bank, "models/handdrum.json"); // load model into slot 1
    res.shiftToNote(bank, pitch);           // shift model 1 to pitch
    res.setRes(bank, bank);                 // set bank 1 to model 1

    res.updateAll(); 
  } else {
    rt_printf("[render.cpp] No models loaded\n");
  }
  
  // res.setResonatorsTest(); // test resonantors functions 

  return true;
}

void render (BelaContext *context, void *userData) { 
  for (unsigned int n = 0; n < context->audioFrames; ++n) {
    float out1 = res.render(0, audioRead(context, n, 0) * input_gain);
    float out2 = res.render(1, audioRead(context, n, 1) * input_gain);
    audioWrite(context, n, 0, out1 * output_gain);
    audioWrite(context, n, 1, out2 * output_gain);
  }
  // res.monitor(); // TODO: Dead code removal?
}

void cleanup (BelaContext *context, void *userData){}
