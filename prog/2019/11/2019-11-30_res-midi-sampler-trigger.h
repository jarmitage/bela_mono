#include <vector>
#include <string>
#include <map>
#include <algorithm>

// Bela deps
#include <Bela.h>
#include <libraries/Midi/Midi.h>

// Lib
#include "lib/Filters/Resonators/Resonators.h"
#include "lib/Devices/Sampler/SampleTriggerDigital.h"
#include "lib/Utils/Pitch.h"

// set to true if using away from web app
bool loadDefaultModels = true;
bool midiTriggerMode   = true;
float input_gain  = 0.125; // MIDI: 0.01
float output_gain = 3.0; // MIDI: 10.0
const char* midiPort = "hw:1,0,0";

// Objects
Resonators res;
SampleTriggerDigital samplers[4];
Midi midi;
Pitch p;

// Resonator pitches & models, MIDI samples and trigger notes
std::vector<std::string> resPitches   = {"c3", "g3", "d4", "a4"};
std::vector<std::string> resModels    = {"handdrum","handdrum","handdrum","handdrum"};
std::vector<std::string> samples      = {"snare", "snare", "snare", "snare"};
std::vector<std::string> midiTriggers = {"c4", "d4", "e4", "f4"};

void midiTriggerSampler(MidiChannelMessage msg) {
  int noteOn = msg.getDataByte(0);
  int velocity = msg.getDataByte(1);
  std::string noteName = p.midiToNoteName(noteOn);
  rt_printf("[render] midiTriggerSampler() note %d %s velocity %d\n", noteOn, noteName.c_str() , velocity);
  std::vector<std::string>::iterator it = std::find(midiTriggers.begin(), midiTriggers.end(), p.midiToNoteName(noteOn));
  if (it != midiTriggers.cend()) {
    int noteIndex = std::distance(midiTriggers.begin(), it);
    samplers[noteIndex].trigger(velocity);
  }
}

void midiMessageCallback(MidiChannelMessage msg, void* arg){
  // msg.prettyPrint();
  if(msg.getType() == kmmNoteOn)
    midiTriggerSampler(msg);
}

void setupMIDI(const char* port){
  midi.readFrom(port);
  midi.writeTo(port);
  midi.enableParser(true);
  midi.setParserCallback(midiMessageCallback, (void*) port);
  rt_printf("[render] setupMIDI() setup on port %s\n", port);
}

bool setup (BelaContext *context, void *userData) {
  setupMIDI(midiPort);

  res.setup(resPitches.size(), context->projectName, context->audioSampleRate, context->audioFrames);

  if (loadDefaultModels) {
    for (int i = 0; i < resPitches.size(); ++i) {
      res.load(i, "data/models/pilot/"+resModels[i]+".json");
      res.shiftToNote(i, resPitches[i]);
      res.setRes(i, i); // set 
      
      samplers[i].setup("data/samples/"+samples[i]+".wav");
    }

    res.updateAll();
  }
  return true;
}

void render (BelaContext *context, void *userData) { 
  for (unsigned int n = 0; n < context->audioFrames; ++n) {
    float out = 0.0;
    for (int i = 0; i < resPitches.size(); ++i)
      out += res.render(i, samplers[i].render(0) * input_gain);
    audioWrite(context, n, 0, out * output_gain);
    audioWrite(context, n, 1, out * output_gain);
  }
}

void cleanup (BelaContext *context, void *userData){}
