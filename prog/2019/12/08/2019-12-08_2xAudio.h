#include <stdio.h>
#include <string>
#include <vector>
#include <Bela.h>
#include "lib/Filters/Resonators/Resonators.h"

/*
  - 2x audio inputs
  - 2x resonators models 
*/

class Program
{
private:
  std::string programName = "2019-12-08_2xAudio";

  Resonators res;
  std::string path = "data/models/pilot/";
  std::vector<std::string> modelPaths = {path+"handdrum.json", path+"handdrum.json"};
  std::vector<std::string> modelPitches = {"c3", "g3"};

  float input_gain  = 0.5;
  float output_gain = 5.0;
public:
  bool setup (BelaContext *context, void *userData) {
    printProgramName();
    res.setup(modelPaths, modelPitches, context->audioSampleRate, context->audioFrames, true);
    return true;
  }
  
  void render (BelaContext *context, void *userData) { 
    for (unsigned int n = 0; n < context->audioFrames; ++n) {
      float out = 0.0;
      for (int i = 0; i < modelPitches.size(); ++i)
        out += res.render(i, audioRead(context, n, i) * input_gain);
      audioWrite(context, n, 0, out * output_gain);
      audioWrite(context, n, 1, out * output_gain);
    }
  }

  void cleanup (BelaContext *context, void *userData){}
  void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
  Program(){}
  ~Program(){}
};
