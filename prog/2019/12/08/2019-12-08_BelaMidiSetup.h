#include <stdio.h>
#include <string>
#include <vector>
#include <Bela.h>
#include <libraries/Midi/Midi.h>

/*

  - simple Midi example

*/

class Program
{
private:
  std::string programName = "2019-12-08_BelaMidiSetup";
  Midi midi;
  const char* midiPort = "hw:1,0,0";

public:
  bool setup (BelaContext *context, void *userData) {
    printProgramName();
    setupMIDI(midiPort);
    return true;
  }
  
  void render (BelaContext *context, void *userData) { 

  }

  void callback(MidiChannelMessage msg, void* arg){
    msg.prettyPrint();
  }

  void setupMIDI(const char* port){
    midi.readFrom(port);
    midi.writeTo(port);
    midi.enableParser(true);
    midi.setParserCallback(midiMessageCallback, (void*) port);
    rt_printf("[render] setupMIDI() setup on port %s\n", port);
  }

  void cleanup (BelaContext *context, void *userData){}
  void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
  Program(){}
  ~Program(){}
};
