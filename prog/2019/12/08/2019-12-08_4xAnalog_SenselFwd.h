#include <stdio.h>
#include <vector>
#include <string>
#include <Bela.h>
#include <libraries/Midi/Midi.h>

#include "lib/Filters/Resonators/Resonators.h"

/*
  - 4x analog inputs
  - 4x resonators models 
  - Midi connected to Sensel
*/

class Program
{
private:
  std::string programName = "2019-12-08_4xAnalog_SenselFwd";

  float input_gain  = 0.5;
  float output_gain = 5.0;

  Resonators res;
  std::string path = "data/models/pilot/";
  std::vector<std::string> modelPaths = {
    path+"handdrum.json", 
    path+"handdrum.json", 
    path+"handdrum.json", 
    path+"handdrum.json"
  };
  std::vector<std::string> modelPitches = {"c3","c3","c3","c3"};

  std::vector<float> piezoInputs = {0,0,0,0};
  float piezoZeroLevel = 0.418;

  int audioPerAnalog = 0;

  Midi midi;
  const char* midiPort = "hw:1,0,0";

public:
  bool setup (BelaContext *context, void *userData, void (*callback)(MidiChannelMessage, void*)) {
    printProgramName();
    setupMIDI(midiPort, callback);
    res.setup(modelPaths, modelPitches, context->audioSampleRate, context->audioFrames, true);
    if(context->analogFrames)
      audioPerAnalog = context->audioFrames / context->analogFrames;
    return true;
  }
  
  void render (BelaContext *context, void *userData) { 
    for (unsigned int n = 0; n < context->audioFrames; ++n) {
      float out = 0.0;
      if(audioPerAnalog && !(n % audioPerAnalog)) {
        for (int i = 0; i < piezoInputs.size(); ++i) {
          piezoInputs[i] = analogRead(context, n/audioPerAnalog, i);
          piezoInputs[i] = (piezoInputs[i] - piezoZeroLevel) * 2;
          out += res.render(i, piezoInputs[i] * input_gain);
        }
      }
      audioWrite(context, n, 0, out * output_gain);
      audioWrite(context, n, 1, out * output_gain);
    }
  }

  void callback(MidiChannelMessage msg, void* arg){
    // msg.prettyPrint();
    if(msg.getType() == kmmNoteOn)
      forwardNoteToGui(msg);
  }

  void forwardNoteToGui(MidiChannelMessage msg) {
    res.wsSendInt(msg.getDataByte(0));
  }

  void setupMIDI(const char* port, void (*callback)(MidiChannelMessage, void*)){
    midi.readFrom(port);
    midi.writeTo(port);
    midi.enableParser(true);
    midi.setParserCallback(callback, (void*) port);
    rt_printf("[render] setupMIDI() setup on port %s\n", port);
  }

  void cleanup (BelaContext *context, void *userData){}
  void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
  Program(){}
  ~Program(){}
};
