#include <stdio.h>
#include <vector>
#include <string>
#include <Bela.h>
#include <libraries/Midi/Midi.h>
// #include <libraries/Gui/Gui.h>

/*

  - gui (pointer to?)

*/

class Program
{
private:
  std::string programName = "2019-12-08_SenselFwd";
  Midi midi;
  const char* midiPort = "hw:1,0,0";

  Gui gui;
public:
  bool setup (BelaContext *context, void *userData, void (*callback)(MidiChannelMessage, void*)) {
    printProgramName();
    setupMIDI(midiPort, callback);
    gui.setup(context->projectName);
    return true;
  }
  
  void render (BelaContext *context, void *userData) { 

  }

  void callback(MidiChannelMessage msg, void* arg){
    msg.prettyPrint();
    if(msg.getType() == kmmNoteOn)
      forwardNoteToGui(msg);
  }

  void forwardNoteToGui(MidiChannelMessage msg){
    gui.sendBuffer(0, msg.getDataByte(0));
  }

  void setupMIDI(const char* port, void (*callback)(MidiChannelMessage, void*)){
    midi.readFrom(port);
    midi.writeTo(port);
    midi.enableParser(true);
    midi.setParserCallback(callback, (void*) port);
    rt_printf("[render] setupMIDI() setup on port %s\n", port);
  }

  void cleanup (BelaContext *context, void *userData){}
  void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
  Program(){}
  ~Program(){}
};
