#include <stdio.h>
#include <vector>
#include <string>

#include <Bela.h>
#include "libraries/Gui/Gui.h"
#include "libraries/Scope/Scope.h"

#include "lib/Utils/JSONUtils.h"
#include "lib/Filters/BiquadEQ.h"
#include "lib/Noise/Noise.h"

class Program {
private:
  std::string programName = "2019-12-13_BiquadEQ_Gui_Scope";

  Gui    gui;
  Scope  scope;

  BiquadEQ eq;
  std::vector<BiquadEQ::BiquadParams> eqParams;

  float in_gain  = 1.0;
  float out_gain = 1.0;
public:
  bool setup (BelaContext *context, void *userData){

    eqParams.push_back({ BiquadEQ::BQ_HIGHPASS,  100,  0.707, 0.0 });
    eqParams.push_back({ BiquadEQ::BQ_HIGHSHELF, 1000, 0.707, 0.0 });
    eq.setup(eqParams, context->audioSampleRate);
    eq.printEQ();

    gui.setup(context->projectName);
    gui.setControlDataCallback([this](JSONObject& root, void* customData)->bool {
      onControl(root);
      return true;
    });
    scope.setup(2, context->audioSampleRate);
    
  	printProgramName();
  	return true;
  }
  void render (BelaContext *context, void *userData){
    for (int n = 0; n < context->audioFrames; ++n) {
      float in  = audioRead(context, n, 0) * in_gain;
      float out = eq.process(in)           * out_gain;
      scope.log(in, out);
      audioWrite(context, n, 0, in);
      audioWrite(context, n, 1, out);
    }
  }
  void updateInOutGain(float _in_gain, float _out_gain) {
    if (in_gain  != _in_gain)  { in_gain  = _in_gain;  }
    if (out_gain != _out_gain) { out_gain = _out_gain; }
    printInOutGain();
  }
  void onControl(JSONObject root) {
    std::wstring cmd;
    if (isCmd(root, cmd)) {
      JSONValue *args = root[L"args"];
      if (isWS(cmd, L"updateEQ"))        onUpdateEQ(args);
      if (isWS(cmd, L"updateBiquad"))    onUpdateBiquad(args);
      if (isWS(cmd, L"updateInOutGain")) onUpdateInOutGain(args);
    }
  }
  void onUpdateEQ(JSONValue *args){
    // JSONObject argsObj = args->AsObject();
    // std::vector<BiquadParams> tmp_params;
    // std::vector<int> types = (int) argsObj[L"types"]->AsArray();
    // float  Fc   = (float)  argsObj[L"Fcs"]->AsArray();
    // float  Q    = (float)  argsObj[L"Qs"]->AsArray();
    // double gain = (double) argsObj[L"gains"]->AsArray();
    // eq.updateEQ(tmp_params);
  }
  void onUpdateBiquad(JSONValue *args){
    JSONObject argsObj = args->AsObject();
    int    i    = (int)    argsObj[L"index"]->AsNumber();
    int    type = (int)    argsObj[L"type"]->AsNumber();
    float  Fc   = (float)  argsObj[L"Fc"]->AsNumber();
    float  Q    = (float)  argsObj[L"Q"]->AsNumber();
    double gain = (double) argsObj[L"gain"]->AsNumber();
    eq.updateBiquad(i, type, Fc, Q, gain);
  }
  void onUpdateInOutGain(JSONValue *args){
    JSONObject argsObj = args->AsObject();
    float _in_gain  = (float) argsObj[L"in"]->AsNumber();
    float _out_gain = (float) argsObj[L"out"]->AsNumber();
    updateInOutGain(_in_gain, _out_gain);
  }
  void printInOutGain(){
    rt_printf("Input gain: %f, Output gain: %f\n", in_gain, out_gain);
  }
  void cleanup (BelaContext *context, void *userData){}
  void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
  Program(){}
  ~Program(){}
};
