#include <stdio.h>
#include <string>
#include <Bela.h>

class Program
{
private:
  std::string programName = "2019-12-13_render_gui_test";
  Gui   *gui;
  Scope *scope;
public:
  bool setup (BelaContext *context, void *userData, Gui &_gui, Scope &_scope){
    gui   = &_gui;
    scope = &_scope;
    gui->setup(context->projectName);
    scope->setup(4, context->audioSampleRate);
  	printProgramName();
  	return true;
  }
  void render (BelaContext *context, void *userData){
    for (int n = 0; n < context->audioFrames; ++n) {
      scope->log(audioRead(context,n, 0));
    }
  }
  void cleanup (BelaContext *context, void *userData){}
  void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
  Program(){}
  ~Program(){}
};
