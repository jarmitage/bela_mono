#include <stdio.h>
#include <string>
#include <iostream>
#include <Bela.h>
#include "libraries/Scope/Scope.h"
#include "libraries/Gui/Gui.h"

#include "lib/Utils/JSONUtils.h"
#include "lib/Compressor/Compressor.h"

class Program
{
private:
    std::string programName = "2019-12-13_Compressor";

    Compressor compressor;
    std::vector<float> input_buffer; 
    std::vector<float> output_buffer;

    Scope scope;
    Gui   gui;

    float in_gain  = 1.0;
    float out_gain = 1.0;

    int gAudioFramesPerAnalogFrame;
public:
    bool setup (BelaContext *context, void *userData){
        
        // Audio buffers
        input_buffer.reserve  (context->audioFrames);
        output_buffer.reserve (context->audioFrames);
        for (int n = 0; n < context->audioFrames; ++n) {
            input_buffer.push_back(0.0);
            output_buffer.push_back(0.0);
        }

        // Gui
        gui.setup(context->projectName);
        gui.setControlDataCallback([this](JSONObject& root, void* customData)->bool {
          onControl(root);
          return true;
        });

        // Scope
        scope.setup(2, context->audioSampleRate);

        // Compressor
        compressor.setup(context->audioSampleRate, context->audioFrames);
        compressor.setThreshold  (-42.0); // dB, max = 0dB
        compressor.setRatio      (4.0);   // 1.0 to 34.0. Feedback approaches infinity:1 as input increases
        compressor.setAttack     (250.0); // ms, 0.1 to 1000
        compressor.setRelease    (75.0);  // ms, 10 to 1000
        compressor.setOutputGain (-9.0);  // dB, -60 to +20 dB
        compressor.setBypass     (false); // set bypass to false
        compressor.printSettings();

        gAudioFramesPerAnalogFrame = context->audioFrames / context->analogFrames;
        
        printProgramName();
        return true;
    }
    void render (BelaContext *context, void *userData){
        for(unsigned int n = 0; n < context->audioFrames; n++)
            input_buffer[n] = audioRead(context, n, 0) * in_gain;

        output_buffer = compressor.process(input_buffer);

        for(unsigned int n = 0; n < context->audioFrames; n++) {

            scope.log(input_buffer[n], output_buffer[n] * out_gain);

            audioWrite(context, n, 0, output_buffer[n] * out_gain);
            audioWrite(context, n, 1, output_buffer[n] * out_gain);
        }
    }
    void onControl(JSONObject root) {
        std::wstring cmd;
        if (isCmd(root, cmd)) {
            JSONValue *args = root[L"args"];
            if (cmd.compare(L"updateCompressor") == 0) onUpdateCompressor(args);
            if (cmd.compare(L"updateInOutGain")  == 0) onUpdateInOutGain(args);
        }
    }
    void onUpdateCompressor(JSONValue *args){
        JSONObject argsObj = args->AsObject();
        
        float t  = (float) argsObj[L"threshold"]->AsNumber();
        float r  = (float) argsObj[L"ratio"]->AsNumber();
        float a  = (float) argsObj[L"attack"]->AsNumber();
        float rl = (float) argsObj[L"release"]->AsNumber();
        float g  = (float) argsObj[L"gain"]->AsNumber();
        float w  = (float) argsObj[L"mix"]->AsNumber();
        
        bool k = (bool) argsObj[L"knee"]->AsBool();
        bool f = (bool) argsObj[L"transfer"]->AsBool();
        bool b = (bool) argsObj[L"bypass"]->AsBool();

        compressor.updateSettings(t, r, a, rl, g, w, k, f, b);
        compressor.printSettings();
    }
    void onUpdateInOutGain(JSONValue *args){
        JSONObject argsObj = args->AsObject();
        float _in_gain  = (float) argsObj[L"in"]->AsNumber();
        float _out_gain = (float) argsObj[L"out"]->AsNumber();

        if (in_gain  != _in_gain)  { in_gain  = _in_gain;  }
        if (out_gain != _out_gain) { out_gain = _out_gain; }
    }
    void cleanup (BelaContext *context, void *userData){}
    void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
    Program(){}
    ~Program(){}
};
