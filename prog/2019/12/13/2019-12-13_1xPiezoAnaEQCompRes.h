#include <stdio.h>
#include <vector>
#include <string>

#include <Bela.h>

#include "lib/Filters/EQ/BiquadEQ.h"
#include "lib/Filters/Resonators/Resonators.h"

#include "lib/Effects/Compressor/Compressor.h"
#include "lib/Effects/Noise/Noise.h"

#include "lib/Utils/JSONUtils.h"
#include "lib/Utils/JSONOnUpdateParsers.h"

class Program {
private:
  std::string programName = "2019-12-13_1xPiezoAnaEQCompRes";
  Gui   *gui;
  Scope *scope;

  // EQ
  BiquadEQ eq;
  std::vector<BiquadEQ::BiquadParams> eqParams;

  // Compressor
  Compressor compressor;
  std::vector<float> input_buffer; 
  std::vector<float> output_buffer;
  std::vector<float> io_gain = {1.0, 5.0};
  
  // Resonators
  Resonators res;
  std::string path = "data/models/pilot/";
  std::vector<std::string> models  = { path+"handdrum.json" };
  std::vector<std::string> pitches = {"c3"};

  // Piezos
  int audioPerAnalog = 0;
  std::vector<float> analogInputs = {0.0};
  float analogZeroLevel = 0.415;

  // JSON Utils
  JSONUtils           json_u;
  JSONOnUpdateParsers json_p;
public:
  bool setup (BelaContext *context, void *userData, Gui &_gui, Scope &_scope) {

    // EQ
    eqParams.push_back({ BiquadEQ::BQ_HIGHPASS,  100,  0.707, 0.0 });
    eqParams.push_back({ BiquadEQ::BQ_HIGHSHELF, 1000, 0.707, 0.0 });
    eq.setup(eqParams, context->audioSampleRate);
    eq.printEQ();

    // Compressor
    compressor.setup(context->audioSampleRate, context->audioFrames);
    compressor.updateSettings(-42, 4, 250, 75, -9, 0.5, false, false, false);
    for (int n = 0; n < context->audioFrames; ++n) {
      input_buffer.push_back(0.0);
      output_buffer.push_back(0.0);
    }
    compressor.printSettings();

    // Gui
    gui = &_gui;
    gui->setup(context->projectName);
    gui->setControlDataCallback([this](JSONObject& root, void* customData)->bool {
      onControl(root);
      return true;
    });
    // gui->sendControl(structToJson(eqParams)); // init by sending params from Bela to JS

    // Scope
    scope = &_scope;
    scope->setup(2, context->audioSampleRate);
    
    // Resonators
    res.setup(models, pitches, context->audioSampleRate, context->audioFrames);
    if(context->analogFrames)
      audioPerAnalog = context->audioFrames / context->analogFrames;

  	printProgramName();
  	return true;
  }
  void render (BelaContext *context, void *userData){
    float eq_out = 0.0;
    for (int n = 0; n < context->audioFrames; ++n) {
      if(audioPerAnalog && !(n % audioPerAnalog)) {
        for (int i = 0; i < analogInputs.size(); ++i) {
          analogInputs[i] = analogRead(context, n/audioPerAnalog, i);
          analogInputs[i] = (analogInputs[i] - analogZeroLevel) * 2 * io_gain[0];
          eq_out = eq.process(analogInputs[i]);
        }
      }
      input_buffer[n] = eq_out;
    }

    output_buffer = compressor.process(input_buffer);

    for (int n = 0; n < context->audioFrames; ++n) {
      float out = res.render(0, output_buffer[n]) * io_gain[1];
      scope->log(analogInputs[0], output_buffer[n], out);
      audioWrite(context, n, 0, analogInputs[0]);
      audioWrite(context, n, 1, out);
    }
  }
  void onControl(JSONObject root) {
    std::wstring cmd;
    if (json_u.isCmd(root, cmd)) {
      JSONValue *args = root[L"args"];
           if (json_u.isWS(cmd, L"updateEQ"))            json_p.onUpdateEQ(eq, args);
      else if (json_u.isWS(cmd, L"updateBiquadEQ"))      json_p.onUpdateBiquadEQ(eq, args);
      else if (json_u.isWS(cmd, L"updateCompressor"))    json_p.onUpdateCompressor(compressor, args);
      else if (json_u.isWS(cmd, L"updateIOGain"))        json_p.onUpdateIOGain(io_gain, args);
      else if (json_u.isWS(cmd, L"updateResModel"))      json_p.onUpdateResModel(res, args);
      else if (json_u.isWS(cmd, L"updateResPitch"))      json_p.onUpdateResPitch(res, args);
      else if (json_u.isWS(cmd, L"updateResResonators")) json_p.onUpdateResResonators(res, args);
    }
  }
  void cleanup (BelaContext *context, void *userData){}
  void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
  Program(){}
  ~Program(){}
};
