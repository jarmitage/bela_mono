#include <stdio.h>
#include <string>
#include <Bela.h>

#include "lib/Effects/Gate/Gate.h"

class Program
{
private:
  std::string programName = "2019-12-20_CuttleNoiseGate";
  Gui   *gui;
  Scope *scope;
  Gate gate;
  float threshold = 0.1;
public:
  bool setup (BelaContext *context, void *userData, Gui &_gui, Scope &_scope){
    gate.setup(threshold, context->analogFrames);

    gui = &_gui;
    scope = &_scope;
    gui->setup(context->projectName);
    scope->setup(4, context->analogSampleRate);
  	printProgramName();
  	return true;
  }
  void render (BelaContext *context, void *userData){
    for (int n = 0; n < context->audioFrames; ++n) {
      float in = audioRead(context, n, 0);
      float gateState = (float) gate.process(in);
      float rms = gate.getRms();
      scope->log(in, rms, gateState);
    }
  }
  void cleanup (BelaContext *context, void *userData){}
  void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
  Program(){}
  ~Program(){}
};
