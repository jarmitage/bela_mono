#include <stdio.h>
#include <string>
#include <Bela.h>
#include "libraries/Gui/Gui.h"

class Program
{
private:
  std::string programName = "ProgramGui";
  Gui gui;
  int sendFreq = 44100;
  int sendCount = 0;
public:
  bool setup (BelaContext *context, void *userData){
    gui.setup(context->projectName);
  	printProgramName();
  	return true;
  }
  void render (BelaContext *context, void *userData){
    for (int n = 0; n < context->audioFrames; ++n) {
      if (++sendCount >= sendFreq){
        testSendControl();
        sendCount = 0;
      }
    }
  }
  void testSendControl() {
    if (gui.isConnected()){
      JSONObject root;
      root[L"test"] = new JSONValue (L"testing");
      JSONValue* value = new JSONValue (root);
      gui.sendControl(value);
      rt_printf("Sending control value\n");
    } else {
      rt_printf("Gui not connected\n");
    }
  }
  void cleanup (BelaContext *context, void *userData){}
  void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
  Program(){}
  ~Program(){}
};
