#include <stdio.h>
#include <vector>
#include <string>
#include <Bela.h>
#include <libraries/Gui/Gui.h>

#include "lib/Sensel/SenselMidi.h"

class Program
{
private:
  std::string programName = "2019-12-12_SenselMidi";
  SenselMidi sensel;

  Gui gui;
public:
  bool setup (BelaContext *context, void *userData, void (*callback)(MidiChannelMessage, void*)) {
    sensel.setup(callback);
    gui.setup(context->projectName);
    printProgramName();
    return true;
  }
  
  void render (BelaContext *context, void *userData) { 

  }

  void onMidi(MidiChannelMessage msg, void* arg){
    sensel.onMidi(msg, arg);
  }

  void cleanup (BelaContext *context, void *userData){}
  void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
  Program(){}
  ~Program(){}
};
