#include <stdio.h>
#include <string>
#include <vector>
#include <Bela.h>
#include "lib/Resonators/Resonators.h"

class Program
{
private:
  std::string programName = "2019-12-12_2xAudio_sendControl";

  Resonators res;
  std::string path = "data/models/pilot/";
  std::vector<std::string> modelPaths = {path+"handdrum.json", path+"handdrum.json"};
  std::vector<std::string> modelPitches = {"c3", "g3"};

  float input_gain  = 0.5;
  float output_gain = 5.0;

  int sendFreq = 44100;
  int sendCount = 0;

  int selectedModel = 0;
  int totalModels = 4;
public:
  bool setup (BelaContext *context, void *userData) {
    printProgramName();
    res.setup(modelPaths, modelPitches, context->audioSampleRate, context->audioFrames, true);
    return true;
  }
  
  void render (BelaContext *context, void *userData) { 
    for (unsigned int n = 0; n < context->audioFrames; ++n) {
      float out = 0.0;
      for (int i = 0; i < modelPitches.size(); ++i)
        out += res.render(i, audioRead(context, n, i) * input_gain);
      audioWrite(context, n, 0, out * output_gain);
      audioWrite(context, n, 1, out * output_gain);
      if (++sendCount >= sendFreq){
        testSendControl();
        sendCount = 0;
      }
    }
  }

  void testSendControl() {
    if (res.wsIsConnected()){
      JSONObject obj;

      if (++selectedModel == totalModels-1) selectedModel = 0;

      obj[L"command"] = new JSONValue (L"selectModel");
      obj[L"model"]   = new JSONValue (selectedModel);

      res.wsSendControl(obj);
      rt_printf("[render.cpp] testSendControl() Sending control value\n");
    } else {
      rt_printf("[render.cpp] testSendControl() Gui not connected\n");
    }
  }

  void cleanup (BelaContext *context, void *userData){}
  void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
  Program(){}
  ~Program(){}
};
