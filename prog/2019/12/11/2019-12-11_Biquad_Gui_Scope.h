#include <stdio.h>
#include <iostream>
#include <string>
#include <Bela.h>
#include "libraries/Gui/Gui.h"
#include "libraries/Scope/Scope.h"
#include "libraries/Biquad/Biquad.h"

class Program {
private:
  std::string programName = "2019-12-11_Biquad_Gui_Scope";
  Gui    gui;
  Scope  scope;

  Biquad bq;
  int    type = 0;
  float  Fc   = 800;
  float  Q    = 0.707;
  double gain = 0.0;

  float in_gain  = 1.0;
  float out_gain = 1.0;
public:
  bool setup (BelaContext *context, void *userData){
    gui.setup(context->projectName);
    gui.setControlDataCallback([this](JSONObject& root, void* customData)->bool {
      onControl(root);
      return true;
    });
    scope.setup(2, context->audioSampleRate);
    bq.setup(Fc, context->audioSampleRate, type, Q, gain);
    printBiquad();
  	printProgramName();
  	return true;
  }
  void render (BelaContext *context, void *userData){
    for (int n = 0; n < context->audioFrames; ++n) {
      float in  = audioRead(context, n, 0) * in_gain;
      float out = bq.process(in)           * out_gain;
      scope.log(in, out);
      audioWrite(context, n, 0, in);
      audioWrite(context, n, 1, out);
    }
  }
  void updateBiquad(int _type, float _Fc, float _Q, double _gain) {
    if (type != _type) { type = _type; bq.setType(type);     }
    if (Fc   != _Fc)   { Fc   = _Fc;   bq.setFc(Fc);         }
    if (Q    != _Q)    { Q    = _Q;    bq.setQ(Q);           }
    if (gain != _gain) { gain = _gain; bq.setPeakGain(gain); }
    printBiquad();
  }
  void updateInOutGain(float _in_gain, float _out_gain) {
    if (in_gain  != _in_gain)  { in_gain  = _in_gain;  }
    if (out_gain != _out_gain) { out_gain = _out_gain; }
    printInOutGain();
  }
  void onControl(JSONObject root) {
    if (root.find(L"command") != root.end() && root[L"command"]->IsString()){
      std::wstring cmd = root[L"command"]->AsString();
      JSONValue *args  = root[L"args"];
      if (cmd.compare(L"updateBiquad")    == 0) onUpdateBiquad(args);
      if (cmd.compare(L"updateInOutGain") == 0) onUpdateInOutGain(args);
    } else {
      rt_printf("[onControl] Received JSON with unknown command name. Did not parse.\n");
    }
  }
  void printObject(JSONObject obj){
    JSONValue *value = new JSONValue (obj);
    const wchar_t* output = value->Stringify().c_str();
    std::wcout << output << "\n";
    std::wcout.flush();
  }
  void onUpdateBiquad(JSONValue *args){
    JSONObject argsObj = args->AsObject();
    int    _type = (int)    argsObj[L"type"]->AsNumber();
    float  _Fc   = (float)  argsObj[L"Fc"]->AsNumber();
    float  _Q    = (float)  argsObj[L"Q"]->AsNumber();
    double _gain = (double) argsObj[L"gain"]->AsNumber();
    updateBiquad(_type, _Fc, _Q, _gain);
  }
  void onUpdateInOutGain(JSONValue *args){
    JSONObject argsObj = args->AsObject();
    float _in_gain  = (float) argsObj[L"in"]->AsNumber();
    float _out_gain = (float) argsObj[L"out"]->AsNumber();
    updateInOutGain(_in_gain, _out_gain);
  }
  void printBiquad() {
    std::string typeS = "";
    switch (type) {
      case 0: typeS = "lowpass";   break;
      case 1: typeS = "highpass";  break;
      case 2: typeS = "bandpass";  break;
      case 3: typeS = "notch";     break;
      case 4: typeS = "peak";      break;
      case 5: typeS = "lowshelf";  break;
      case 6: typeS = "highshelf"; break;
    }
    rt_printf("[Biquad] Type %s, Fc %f, Q %f, gain %f\n", typeS.c_str(), Fc, Q, gain);
  }
  void printInOutGain(){
    rt_printf("Input gain: %f, Output gain: %f\n", in_gain, out_gain);
  }
  void cleanup (BelaContext *context, void *userData){}
  void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
  Program(){}
  ~Program(){}
};
