#include <stdio.h>
#include <string>
#include <vector>
#include <Bela.h>
#include <libraries/Scope/Scope.h>

#include "lib/Filters/EQ3B.h"
#include "lib/Resonators/Resonators.h"

class Program
{
private:
  std::string programName = "2019-12-08_4xAnalog_EQ3B";

  float input_gain  = 1;
  float output_gain = 5.0;

  Resonators res;
  std::string path = "data/models/pilot/";
  std::vector<std::string> models = {
    path+"handdrum.json"
    ,path+"metallic.json"
    ,path+"handdrum.json"
    ,path+"metallic.json"
  };
  std::vector<std::string> pitches = {
    "d4"
    ,"c3"
    ,"f3"
    ,"a3"
  };
  std::vector<EQ3B> eqs = {};
  Scope scope;
  std::vector<float> piezoInputs = {0,0,0,0};
  float piezoZeroLevel = 0.415;

  int audioPerAnalog = 0;
public:
  bool setup (BelaContext *context, void *userData) {
    scope.setup(4, context->audioSampleRate);
    eqs.reserve(pitches.size());
    for (int i = 0; i < pitches.size(); ++i){
      EQ3B tmp_eq;
      tmp_eq.setup(800, 4000, 0.5, 2.5, 5.0, context->audioSampleRate);
      eqs.push_back(tmp_eq);
    }
    res.setup(models, pitches, context->audioSampleRate, context->audioFrames, true);
    if(context->analogFrames)
      audioPerAnalog = context->audioFrames / context->analogFrames;
    printProgramName();
    return true;
  }
  
  void render (BelaContext *context, void *userData) { 
    for (unsigned int n = 0; n < context->audioFrames; ++n) {
      float out = 0.0;
      if(audioPerAnalog && !(n % audioPerAnalog)) {
        for (int i = 0; i < piezoInputs.size(); ++i) {
          piezoInputs[i] = analogRead(context, n/audioPerAnalog, i);
          piezoInputs[i] = (piezoInputs[i] - piezoZeroLevel) * 2 * input_gain;
        }
      }
      for (int i = 0; i < pitches.size(); ++i) {
        piezoInputs[i] = (float)eqs[i].process((double)piezoInputs[i]);
        out += res.render(i, piezoInputs[i]);
      }
      // scope.log(piezoInputs[0], piezoInputs[1], piezoInputs[2], piezoInputs[3]);
      audioWrite(context, n, 0, out * output_gain);
      audioWrite(context, n, 1, out * output_gain);
    }
  }

  void cleanup (BelaContext *context, void *userData){}
  void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
  Program(){}
  ~Program(){}
};
