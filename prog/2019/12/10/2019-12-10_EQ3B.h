#include <stdio.h>
#include <string>
#include <Bela.h>
#include "libraries/Scope/Scope.h"
#include "lib/Filters/EQ3B.h"

class Program
{
private:
  std::string programName = "2019-12-10_EQ3B";
  EQ3B eq1;
  EQ3B eq2;
  Scope scope;
  int audioPerAnalog = 0;
  float in = 0.0;
  float input_gain = 2.0;
  float out1 = 0.0;
  float out2 = 0.0;
  float analogZeroLevel = 0.415;
public:
  bool setup (BelaContext *context, void *userData){
    scope.setup (4,context->audioSampleRate);
    eq1.setup (1000, 4000, 0.01, 1.0, 20.0, context->audioSampleRate);
    eq2.setup (1000, 4000, 0.01, 4.0, 20.0, context->audioSampleRate);
    if(context->analogFrames)
      audioPerAnalog = context->audioFrames / context->analogFrames;
  	printProgramName();
  	return true;
  }
  void render (BelaContext *context, void *userData){
    for (int n = 0; n < context->audioFrames; ++n) {
      if(audioPerAnalog && !(n % audioPerAnalog))
        in = (analogRead(context, n/audioPerAnalog, 0) - analogZeroLevel) * 2 * input_gain;
      out1 = (float)eq1.process((double)in);
      out2 = (float)eq2.process((double)in);
      scope.log(in, out1, out2);
    }
  }
  void cleanup (BelaContext *context, void *userData){}
  void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
  Program(){}
  ~Program(){}
};
