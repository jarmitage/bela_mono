#include <stdio.h>
#include <string>
#include <Bela.h>
#include "libraries/Scope/Scope.h"
#include "lib/Filters/EQ3B.h"
#include "lib/Resonators/Resonators.h"

class Program
{
private:
  std::string programName = "2019-12-10_EQ3B_2xRes";
  Resonators res;
  Scope scope;
  EQ3B eq1;
  std::string path = "data/models/pilot/";
  std::vector<std::string> models = {
    path+"handdrum.json",
    path+"handdrum.json"
  };
  std::vector<std::string> pitches = {"c3", "c3"};
  float in = 0.0;
  float input_gain = 2.0;
  float in_eq = 0.0;
  float out1 = 0.0;
  float out2 = 0.0;
  float analogZeroLevel = 0.415;
  int audioPerAnalog = 0;
public:
  bool setup (BelaContext *context, void *userData){
    scope.setup (4,context->audioSampleRate);
    res.setup(models, pitches, context->audioSampleRate, context->audioFrames, false);
    eq1.setup (800, 3000, 0.01, 10.0, 30.0, context->audioSampleRate);
    if(context->analogFrames)
      audioPerAnalog = context->audioFrames / context->analogFrames;
  	printProgramName();
  	return true;
  }
  void render (BelaContext *context, void *userData){
    for (int n = 0; n < context->audioFrames; ++n) {
      if(audioPerAnalog && !(n % audioPerAnalog)) {
        in = (analogRead(context, n/audioPerAnalog, 0) - analogZeroLevel) * 2 * input_gain;
        in_eq = (float)eq1.process((double)in);
      }
      out1 = res.render(0, in);
      out2 = res.render(1, in_eq);
      scope.log(in, in_eq, out1, out2);
      audioWrite(context, n, 0, out1);
      audioWrite(context, n, 1, out2);
    }
  }
  void cleanup (BelaContext *context, void *userData){}
  void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
  Program(){}
  ~Program(){}
};
