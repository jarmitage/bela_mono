#include <stdio.h>
#include <string>
#include <vector>
#include <Bela.h>
#include "libraries/Gui/Gui.h"
#include "libraries/Scope/Scope.h"
#include "lib/Filters/EQ3B.h"
// #include JSON?

class Program
{
private:
  std::string programName = "2019-12-10_EQ3B";
  std::vector<EQ3B> eqs;
  int totalEQs = 2;
  Scope scope;
  Gui gui;
  int audioPerAnalog = 0;
  float in = 0.0;
  float input_gain = 2.0;
  std::vector<float> outs;
  float out1 = 0.0;
  float out2 = 0.0;
  float analogZeroLevel = 0.415;
public:
  bool setup (BelaContext *context, void *userData){
    scope.setup (4,context->audioSampleRate);
    eqs.reserve(totalEQs);
    outs.reserve(eqs.size());
    for (int i = 0; i < eqs.size(); ++i) eqs[i].setup();
    gui.setup (context->projectName);
    gui.setControlDataCallback([this](const char* buf, int bufLen, void* customData)->bool {
      onControl(buf, bufLen);
      return true;
    });
    if(context->analogFrames)
      audioPerAnalog = context->audioFrames / context->analogFrames;
  	printProgramName();
  	return true;
  }
  void render (BelaContext *context, void *userData){
    for (int n = 0; n < context->audioFrames; ++n) {
      if(audioPerAnalog && !(n % audioPerAnalog))
        in = (analogRead(context, n/audioPerAnalog, 0) - analogZeroLevel) * 2 * input_gain;
      for (int i = 0; i < eqs.size(); ++i)
        outs[i] = (float)eqs[i].process((double)in);
      scope.log(in, out[0], outs[1]);
    }
  }
  void onControl(const char* buf, int bufLen) {
    JSONValue *value = parseJSON(buf);
    JSONObject root = value->AsObject();
    if (root.find(L"event") != root.end() && root[L"event"]->IsString()){
      std::wstring event = root[L"event"]->AsString();
      if (event.compare(L"connection-reply") == 0)
        gui.isConnected = true;
    } else if (root.find(L"command") != root.end() && root[L"command"]->IsString()){
      std::wstring cmd = root[L"command"]->AsString();
      JSONValue *args = value->Child(L"args");
      if (cmd.compare(L"setFreqs") == 0) onSetFreqs(args);
      if (cmd.compare(L"setGains") == 0) onSetGains(args);
    } else {
      rt_printf("[EQ3B] Received JSON with unknown command name. Did not parse.\n");
    }
    delete value;
  }
  JSONValue* parseJSON(const char* buf){
    JSONValue *value = JSON::Parse(buf);
    if (value == NULL || !value->IsObject()){
      fprintf(stderr, "[EQ3B] Could not parse JSON:\n%s\n", buf);
      return nullptr;
    }
    return value;
  }
  void onSetFreqs(JSONValue *args){
    JSONObject argsObj = args->AsObject();
    int index = (int) argsObj[L"index"]->AsNumber();
    double low  = (double) argsObj[L"low"]->AsNumber();
    double high = (double) argsObj[L"high"]->AsNumber();
    eqs[index].setFreqs(low, high);
  }
  void onSetGains(JSONValue *args){
    JSONObject argsObj = args->AsObject();
    int index = (int) argsObj[L"index"]->AsNumber();
    double low  = (double) argsObj[L"low"]->AsNumber();
    double mid  = (double) argsObj[L"mid"]->AsNumber();
    double high = (double) argsObj[L"high"]->AsNumber();
    eqs[index].setGains(low, mid, high);
  }
  void cleanup (BelaContext *context, void *userData){}
  void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
  Program(){}
  ~Program(){}
};
