#include <stdio.h>
#include <vector>
#include <string>

#include <Bela.h>

#include "lib/Filters/EQ/BiquadEQ.h"
#include "lib/Filters/Resonators/Resonators.h"

#include "lib/Utils/JSONUtils.h"
#include "lib/Utils/JSONOnUpdateParsers.h"

class Program {
private:
  std::string programName = "2019-12-14_4xPiezoAnaEQRes";
  Gui   *gui;
  Scope *scope;

  // EQ
  std::vector<BiquadEQ> eqs;
  std::vector<BiquadEQ::BiquadParams> eqParams {
    { BiquadEQ::BQ_HIGHPASS,  100,  0.1, -24.0 },
    { BiquadEQ::BQ_HIGHSHELF, 2000, 0.5, 3.0 }
  };
  std::vector<float> eqsOut {0.0, 0.0, 0.0, 0.0};

  // Resonators
  Resonators res;
  std::string path  = "data/models/pilot/";
  std::string model = "handdrum.json";
  std::vector<std::string> models  { path+model, path+model, path+model, path+model };
  std::vector<std::string> pitches { "g4", "c3", "a3", "d4" };

  // Piezos
  int audioPerAnalog = 0;
  std::vector<float> analogInputs {0.0, 0.0, 0.0, 0.0};
  float analogZeroLevel = 0.415;
  std::vector<float> io_gain {1.0, 5.0};

  // JSON Utils
  JSONUtils           json_u;
  JSONOnUpdateParsers json_p;
public:
  bool setup (BelaContext *context, void *userData, Gui &_gui, Scope &_scope) {

    // EQs
    for (int i = 0; i < eqsOut.size(); ++i) {
      BiquadEQ tmp_eq;
      tmp_eq.setup(eqParams, context->audioSampleRate);
      eqs.push_back(tmp_eq);
      eqs[i].printEQ();
    }

    // Gui
    gui = &_gui;
    gui->setup(context->projectName);
    gui->setControlDataCallback([this](JSONObject& root, void* customData)->bool {
      onControl(root);
      return true;
    });

    // Scope
    scope = &_scope;
    scope->setup(4, context->audioSampleRate);
    
    // Resonators
    res.setup(models, pitches, context->audioSampleRate, context->audioFrames);
    if(context->analogFrames)
      audioPerAnalog = context->audioFrames / context->analogFrames;

  	printProgramName();
  	return true;
  }
  void render (BelaContext *context, void *userData){
    for (int n = 0; n < context->audioFrames; ++n) {
      if(audioPerAnalog && !(n % audioPerAnalog)) {
        for (int i = 0; i < analogInputs.size(); ++i) {
          analogInputs[i] = analogRead(context, n/audioPerAnalog, i);
          analogInputs[i] = (analogInputs[i] - analogZeroLevel) * 2 * io_gain[0];
          eqsOut[i] = eqs[i].process(analogInputs[i]);
        }
      }
      float out = 0.0;
      for (int i = 0; i < models.size(); ++i)
        out += res.render(i, eqsOut[i]);
      // scope->log(analogInputs[0], eqsOut[0], out * io_gain[1]);
      audioWrite(context, n, 0, out * io_gain[1]);
      audioWrite(context, n, 1, out * io_gain[1]);
    }
  }
  void onControl(JSONObject root) {
    std::wstring cmd;
    if (json_u.isCmd(root, cmd)) {
      JSONValue *args = root[L"args"];
           if (json_u.isWS(cmd, L"updateIOGain"))        json_p.onUpdateIOGain(io_gain, args);
      // else if (json_u.isWS(cmd, L"updateEQ"))            json_p.onUpdateEQ(eq, args);
      // else if (json_u.isWS(cmd, L"updateBiquadEQ"))      json_p.onUpdateBiquadEQ(eq, args);
      else if (json_u.isWS(cmd, L"updateResModel"))      json_p.onUpdateResModel(res, args);
      else if (json_u.isWS(cmd, L"updateResPitch"))      json_p.onUpdateResPitch(res, args);
      else if (json_u.isWS(cmd, L"updateResResonators")) json_p.onUpdateResResonators(res, args);
    }
  }
  void cleanup (BelaContext *context, void *userData){}
  void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
  Program(){}
  ~Program(){}
};
