#include <stdio.h>
#include <string>
#include <vector>
#include <Bela.h>
#include <libraries/Scope/Scope.h>
#include "lib/Resonators/Resonators.h"

class Program
{
private:
  std::string programName = "2019-12-09_2xAudio_2xAnalog_Scope";

  Resonators res;
  std::string path = "data/models/pilot/";
  std::vector<std::string> modelPaths = {
    path+"handdrum.json", 
    path+"handdrum.json", 
    path+"handdrum.json", 
    path+"handdrum.json"
  };
  std::vector<std::string> modelPitches = {"c3","c3","c3","c3"};

  float analog_input_gain = 2;
  float audio_input_gain  = 0.5;
  float output_gain = 5.0;

  Scope scope;
  int audioPerAnalog = 0;
  std::vector<float> audioInputs  = {0.0, 0.0};
  std::vector<float> analogInputs = {0.0, 0.0};
  float analogZeroLevel = 0.415;
public:
  bool setup (BelaContext *context, void *userData) {
    printProgramName();
    scope.setup(4, context->audioSampleRate);
    res.setup(modelPaths, modelPitches, context->audioSampleRate, context->audioFrames, true);
    if(context->analogFrames)
      audioPerAnalog = context->audioFrames / context->analogFrames;
    rt_printf("[setup()] audio: %d, analog: %d, audioPerAnalog: %d\n", context->audioFrames, context->analogFrames, audioPerAnalog);
    return true;
  }
  
  void render (BelaContext *context, void *userData) { 
    for (unsigned int n = 0; n < context->audioFrames; ++n) {
      float out = 0.0;
      if(audioPerAnalog && !(n % audioPerAnalog)) {
        for (int i = 0; i < analogInputs.size(); ++i) {
          analogInputs[i] = analogRead(context, n/audioPerAnalog, i);
          analogInputs[i] = (analogInputs[i] - analogZeroLevel) * 2 * analog_input_gain;
          out += res.render(i, analogInputs[i]);
        }
      }
      for (int i = 0; i < audioInputs.size(); ++i) {
        audioInputs[i] = audioRead(context, n, i);
        out += res.render(i + analogInputs.size(), audioInputs[i] * audio_input_gain);
      }
      scope.log(analogInputs[0], analogInputs[1], audioInputs[0], audioInputs[1]);
      audioWrite(context, n, 0, out * output_gain);
      audioWrite(context, n, 1, out * output_gain);
    }
  }

  void cleanup (BelaContext *context, void *userData){}
  void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
  Program(){}
  ~Program(){}
};
