#include <stdio.h>
#include <string>
#include <vector>
#include <Bela.h>
#include <libraries/Scope/Scope.h>
#include "lib/Resonators/Resonators.h"

class Program
{
private:
  std::string programName = "2019-12-09_2xAudio_Scope";

  Resonators res;
  std::string path = "data/models/pilot/";
  std::vector<std::string> modelPaths = {path+"handdrum.json", path+"handdrum.json"};
  std::vector<std::string> modelPitches = {"c3", "g3"};

  Scope scope;
  std::vector<float> audioInputs  = {0.0, 0.0};
  float input_gain  = 0.5;
  float output_gain = 5.0;
public:
  bool setup (BelaContext *context, void *userData) {
    printProgramName();
    scope.setup(4, context->audioSampleRate);
    res.setup(modelPaths, modelPitches, context->audioSampleRate, context->audioFrames, true);
    return true;
  }
  
  void render (BelaContext *context, void *userData) { 
    for (unsigned int n = 0; n < context->audioFrames; ++n) {
      float out = 0.0;
      for (int i = 0; i < audioInputs.size(); ++i){
        audioInputs[i] = audioRead(context, n, i);
        out += res.render(i, audioInputs[i] * input_gain);
      }
      scope.log(audioInputs[0], audioInputs[1]);
      audioWrite(context, n, 0, out * output_gain);
      audioWrite(context, n, 1, out * output_gain);
    }
  }

  void cleanup (BelaContext *context, void *userData){}
  void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
  Program(){}
  ~Program(){}
};
