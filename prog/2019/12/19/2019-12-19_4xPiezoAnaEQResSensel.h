#include <stdio.h>
#include <vector>
#include <string>

#include <Bela.h>

#include "lib/Filters/EQ/BiquadEQ.h"
#include "lib/Filters/Resonators/Resonators.h"

#include "lib/Devices/Sensel/SenselMidiGuiSampler.h"

#include "lib/Utils/JSONUtils.h"
#include "lib/Utils/JSONOnUpdateParsers.h"

class Program {
private:
  std::string programName = "2019-12-19_4xPiezoAnaEQResSensel";
  Gui   *gui;
  Scope *scope;

  SampleTriggerDigital sampler;
  std::string ui_confirm = "data/samples/ui/digi_plink.wav";

  SenselMidiGuiSampler sensel;
  std::string ui_click = "data/samples/ui/ui_01.wav";
  int ui_velocity = 32;

  // Piezos
  int audioPerAnalog = 0;
  std::vector<float> analogInputs {0.0, 0.0, 0.0, 0.0};
  float analogZeroLevel = 0.415;
  std::vector<float> io_gain {1.0, 5.0};

  // EQ
  std::vector<BiquadEQ> eqs;
  std::vector<BiquadEQ::BiquadParams> eqParams {
    { BiquadEQ::BQ_HIGHPASS,  100,  0.1, -24.0 },
    { BiquadEQ::BQ_HIGHSHELF, 2000, 0.5,  3.0  }
  };
  std::vector<float> eqsOut { 0.0, 0.0, 0.0, 0.0 };

  // Resonators
  Resonators res;
  std::string path  = "data/models/pilot/";
  std::string model = "handdrum.json";
  std::vector<std::string> models  { path+model, path+model, path+model, path+model };
  std::vector<std::string> pitches { "c3", "a3", "g4", "d5" };

  // JSON Utils
  JSONUtils           json_u;
  JSONOnUpdateParsers json_p;
public:
  bool setup (BelaContext *context, void *userData, Gui &_gui, Scope &_scope, void (*callback)(MidiChannelMessage, void*)) {

    // EQs
    for (int i = 0; i < eqsOut.size(); ++i) {
      BiquadEQ tmp_eq;
      tmp_eq.setup(eqParams, context->audioSampleRate);
      eqs.push_back(tmp_eq);
      eqs[i].printEQ();
    }

    // Gui
    gui = &_gui;
    gui->setup(context->projectName);
    gui->setControlDataCallback([this](JSONObject& root, void* customData)->bool {
      onControl(root);
      return true;
    });
    
    // Sampler
    sampler.setup(ui_confirm);

    // Sensel
    sensel.setup(_gui, callback, ui_click, ui_velocity);

    // Scope
    scope = &_scope;
    scope->setup(4, context->audioSampleRate);
    
    // Resonators
    res.setup(models, pitches, context->audioSampleRate, context->audioFrames);

    if(context->analogFrames) audioPerAnalog = context->audioFrames / context->analogFrames;
  	printProgramName();
  	return true;
  }
  void render (BelaContext *context, void *userData){
    for (int n = 0; n < context->audioFrames; ++n) {
      if(audioPerAnalog && !(n % audioPerAnalog)) {
        for (int i = 0; i < analogInputs.size(); ++i) {
          analogInputs[i] = analogRead(context, n/audioPerAnalog, i);
          analogInputs[i] = (analogInputs[i] - analogZeroLevel) * 2 * io_gain[0];
          if (analogInputs[i] > 1) analogInputs[i] = 0.9999; // hard limit
          eqsOut[i] = eqs[i].process(analogInputs[i]);
        }
      }
      float out = 0.0;
      for (int i = 0; i < models.size(); ++i)
        out += res.render(i, eqsOut[i]);

      out *= io_gain[1];
      out += sensel.render();
      out += sampler.render(0);
      if (out > 1) out = 0.9999; // hard limit
      audioWrite(context, n, 0, out);
      audioWrite(context, n, 1, out);
      // scope->log(analogInputs[0], eqsOut[0], out * io_gain[1]);
    }
  }
  void onControl(JSONObject root) {
    std::wstring cmd;
    if (json_u.isCmd(root, cmd)) {
      JSONValue *args = root[L"args"];
           if (json_u.isWS(cmd, L"updateGain"))        json_p.onUpdateGain(io_gain, args);
      // else if (json_u.isWS(cmd, L"updateEQ"))            json_p.onUpdateEQ(eq, args);
      // else if (json_u.isWS(cmd, L"updateBiquadEQ"))      json_p.onUpdateBiquadEQ(eq, args);
      else if (json_u.isWS(cmd, L"updateResModel"))      {
        json_p.onUpdateResModel(res, args);
        sampler.trigger(32);
      }
      else if (json_u.isWS(cmd, L"updateResPitch"))      json_p.onUpdateResPitch(res, args);
      else if (json_u.isWS(cmd, L"updateResResonators")) json_p.onUpdateResResonators(res, args);
    }
  }
  void onMidi(MidiChannelMessage msg, void* arg){
    sensel.onMidi(msg, arg);
  }
  void cleanup (BelaContext *context, void *userData){}
  void printProgramName(){printf("[BelaProgram] %s\n", programName.c_str());}
  Program(){}
  ~Program(){}
};
