var r = new Rune({
  container: "#sketch",
  width:  800,
  height: 400
})

// dimensions 
// ----------
let dims = {
  d: 100, // diameter
  m: 4   // mount points
  // ring-plate spacing?
}
// sub-components 
// --------------
let hole = { r: 4 /* radius */ }
let hook = { w: 5 /* width  */ }
// components 
// ----------
let base = {
  d:     dims.d,
  holes: dims.m,
  draw: (o, style, grp) => {
    r.circle(o, o, this.d, grp)
      .strokeWidth(style.sw).stroke(style.sc).fill(style.f)
  }
}
let ring = {
  d:     dims.d,
  // w:     ??, // width
  holes: dims.m,
  hooks: dims.m
}
let plate = {
  d:     dims.d,// * ??, // ring.w?
  holes: dims.m
}
// groups
// ------
let grps = {
  all: r.group(dims.d, dims.d)
}
// layouts (viewing vs. making)
// -------
let layouts = {
  view: {
    style: {
      sw: 0.50, // strokeWidth
      sc: new Rune.Color(0,0,0,255), // stroke color
      f:  new Rune.Color(0,0,0,0)    // fill
    }
  },
  make: {
    style: {
      sw: 0.01, // strokeWidth
      sc: new Rune.Color(255,0,0,255), // stroke color
      f:  new Rune.Color(0,0,0,0)      // fill
    }
  }
}
// tool
// ----
let tool = {
  parts: {
    base:  base,
    ring:  ring,
    plate: plate
  },
  layouts: layouts,
  groups:  grps,
  debug: () => {
    console.log(this)
  },
  draw: (layout) => {
    let style
    if      (layout==='view') style = this.layouts.view
    else if (layout==='make') style = this.layouts.make
    base.draw(0, style, this.groups.all)
  }
}
// drawing
// -------
// drawTool = (tool, style) => {
//   drawBase(tool.base, style)
//   // ring
//   r.circle(0, 0, base.d, grps.all)
//     .strokeWidth(style.sw).stroke(style.sc).fill(style.f)
// }

// base.draw(0, layouts.)
tool.debug()
// tool.draw('view')

// drawTool(tool, layouts.view.style)
// drawBase = (base, style) => {
//   r.circle(0, 0, base.d, grps.all)
//     .strokeWidth(style.sw).stroke(style.sc).fill(style.f)
// }


// editing
// drawFrame(style.sw, frame.c)
// drawBars(style.sw, bar.c)
// scaleGrp(grp, 2)

// cutting
// drawFrame(style.lc, style.cut)
// drawBars(style.lc, style.cut)
// scalePpi(72)

r.draw()
