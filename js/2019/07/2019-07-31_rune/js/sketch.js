class Tool {
  constructor(container, width, height) {
    // ------------------------------
    //   globals
    // ------------------------------
    this.container = container
    this.w = width
    this.h = height
    this._layout
    // ------------------------------
    //   rune
    // ------------------------------
    this.r = new Rune({
      container: this.container,
      width:     this.w,
      height:    this.h
    })
    // ------------------------------
    //   dimensions
    // ------------------------------
    this.dims = {
      d: 230, // diameter
      m: 8    // mount points
      // ring-plate spacing?
    }
    // ------------------------------
    //   sub-components 
    // ------------------------------
    this.hole = {
      r: 4, // radius
      draw: (x, y, n, s, g) => {
        for (let i = 0; i < n; i++) {
          this.r.circle(x, y, this.hole.r, g)
            .strokeWidth(s.sw).stroke(s.sc).fill(s.f)
            .rotate(i * (360/n))
        }
      }
    }
    this.hook = {
      w: [15, 10],
      h: [5,  5],
      drawInner: (x, y, n, s, g) => {
        for (let i = 0; i < n; i++) {
          let x_  = x - 5
          let x__ = x - 10
          this.r.rect(x_, y, this.hook.h[0], this.hook.w[0], g)
            .strokeWidth(s.sw).stroke(s.sc).fill(s.f)
            .rotate(i * (360/n) + (360/n/2))
          this.r.rect(x__, y, this.hook.w[1], this.hook.h[1], g)
            .strokeWidth(s.sw).stroke(s.sc).fill(s.f)
            .rotate(i * (360/n) + (360/n/2))
        }
      },
      drawOuter: (x, y, n, s, g) => {
        for (let i = 0; i < n; i++) {
          this.r.rect(x, y, this.hook.w[1], this.hook.h[1], g)
            .strokeWidth(s.sw).stroke(s.sc).fill(s.f)
            .rotate(i * (360/n) + (360/n/2))
          this.r.rect(x, y, this.hook.h[0], this.hook.w[0], g)
            .strokeWidth(s.sw).stroke(s.sc).fill(s.f)
            .rotate(i * (360/n) + (360/n/2))
        }
      }
    }
    // ------------------------------
    //   components 
    // ------------------------------
    this.base = {
      r:     this.dims.d / 2,
      holes: this.dims.m,
      draw: (l) => {
        let s = l.style, g = l.group, c = l.coord
        
        this.r.circle(c.o.x, c.o.y, this.base.r, g)
          .strokeWidth(s.sw).stroke(s.sc).fill(s.f)

        // holes
        let x = c.o.x + this.base.r - (this.ring.w/2)
        this.hole.draw(x, c.o.y, this.base.holes, s, g)
      }
    }
    this.ring = {
      r:     this.dims.d / 2,
      w:     20, // width
      holes: this.dims.m,
      hooks: this.dims.m,
      draw: (l) => {
        let s = l.style, g = l.group, c = l.coord

        // outer
        this.r.circle(c.o.x, c.o.y, this.ring.r, g)
          .strokeWidth(s.sw).stroke(s.sc).fill(s.f) 

        // inner
        let innerR = this.ring.r - this.ring.w
        this.r.circle(c.o.x, c.o.y, innerR, g)
          .strokeWidth(s.sw).stroke(s.sc).fill(s.f)

        // holes
        let x = c.o.x + this.ring.r - (this.ring.w/2)
        this.hole.draw(x, c.o.y, this.base.holes, s, g)
        this.hook.drawInner(x, c.o.y, this.plate.holes, s, g)
      }
    }
    this.plate = {
      r:     75,
      holes: this.dims.m,
      draw: (l) => {
        let s = l.style, g = l.group, c = l.coord

        this.r.circle(c.o.x, c.o.y, this.plate.r, g)
          .strokeWidth(s.sw).stroke(s.sc).fill(s.f)

        // hooks
        let x = c.o.x + this.plate.r - (this.ring.w/2)
        this.hook.drawOuter(x, c.o.y, this.plate.holes, s, g)
      }
    }
    // ------------------------------
    //   parts
    // ------------------------------
    this.parts = {
      base:  this.base,
      ring:  this.ring,
      plate: this.plate
    }
    // ------------------------------
    //   layouts
    // ------------------------------
    this.layouts = {
      view: {
        style: {
          sw: 0.75, // strokeWidth
          sc: new Rune.Color(0,0,0,255), // stroke color
          f:  new Rune.Color(0,0,0,0)    // fill
        },
        group: this.r.group(250, 250),
        coord: {
          o: {x: 0, y: 0} // origin
        }
      },
      make: {
        style: {
          sw: 0.01, // strokeWidth
          sc: new Rune.Color(255,0,0,255), // stroke color
          f:  new Rune.Color(0,0,0,0)      // fill
        },
        group: this.r.group(250, 250),
        coord: {
          // o: {x: this.dims.d/2, y: this.dims.d/2} // origin
          o: {x: 0, y: 0} // origin
        }
      }
    }
    // ------------------------------
    //   tool
    // ------------------------------
    this.tool = {
      draw: () => {
        let layout = this.getLayout()
        this.parts.base.draw(layout)
        this.parts.ring.draw(layout)
        this.parts.plate.draw(layout)
      }
    }
    // ------------------------------
    //   drawing
    // ------------------------------
    this.draw = (_layout) => {
      this._layout = _layout
      this.tool.draw()
      this.r.draw()
    }
    this.scale = (f) => {
      let group = this.getLayout().group
      group.scale(f)
      this.r.draw()
    }
    this.scalePpi = (p) => this.scale (p / 25.4)
    this.getLayout = () => {
      if      (this._layout==='view') return this.layouts.view
      else if (this._layout==='make') return this.layouts.make
    }
  }
}

let tool = new Tool('#sketch', 1000, 1000)
tool.draw('view')
// tool.scalePpi(72)
tool.scale(2)

// tool.draw('make')
