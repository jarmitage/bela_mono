var r = new Rune({
  container: "#sketch",
  width:  800,
  height: 400
})

let style = {
  'sw': 0.5, // strokeWidth
  'lc': 0.01, // strokeWidth for laser cut
  'cut': new Rune.Color(255,0,0,255),
  'raster': new Rune.Color(0,0,255,255)
}

let bar = {
  'w': 35,
  'h': 150,
  'c': new Rune.Color(0,0,0,255),
  'hole':  {
    'n': 2,
    'r': 3,
    'yOff': 10
  }
}

let frame = {
  'h': 12,
  'bars': 4,
  'c': new Rune.Color(0,0,0,255),
  'x':   100,
  'y':   50,
  'off': 14,
  'hole':  {
    'n': 2,
    'r': 4,
    'yOff': bar.hole.yOff
  }
}

let gBars = r.group(frame.x, frame.y)
let gBarHoles = r.group(frame.x, frame.y)

let gFrame = r.group(frame.x, frame.y)
let gFrameHoles = r.group(frame.x, frame.y)

drawBars = (sw, c) => {

  let holesp = bar.w / (bar.hole.n + 1);

  // marimba bars
  for (var i = 0; i < frame.bars; i++) {

    let barsp = bar.w*i + frame.off*i;

    r.rect(barsp, 0, bar.w, bar.h, gBars)
      .strokeWidth(sw)
      .stroke(c)
      .fill(new Rune.Color (0,0,0,0))

    // holes

    for (var h = 0; h < bar.hole.n; h++) {

      let hole = holesp * (h + 1) + barsp;

      r.circle(hole, bar.hole.yOff, bar.hole.r, gBarHoles)
        .strokeWidth(sw)
        .stroke(c)
        .fill(new Rune.Color (0,0,0,0))

      r.circle(hole, bar.h - bar.hole.yOff, bar.hole.r, gBarHoles)
        .strokeWidth(sw)
        .stroke(c)
        .fill(new Rune.Color (0,0,0,0))
        
    }

  }

}

drawFrame = (sw, c) => {

  r.rect( -frame.off, 0,
          frame.bars*bar.w + frame.bars*frame.off + frame.off, 
          bar.h, 
          gFrame)
    .strokeWidth(sw)
    .stroke(c)
    .fill(new Rune.Color (0,0,0,0))

  r.rect( 0, frame.hole.yOff*2,
          frame.bars*bar.w + frame.bars*frame.off - frame.off, 
          bar.h - frame.hole.yOff * 4,
          gFrame)
    .strokeWidth(sw)
    .stroke(c)
    .fill(new Rune.Color (0,0,0,0))

  // holes

  let holesp = bar.w + frame.off

  for (var i = 0; i < frame.bars + 1; i++) {

    let hole = (holesp*i) - (frame.off/2)

    r.circle(hole, frame.hole.yOff, frame.hole.r, gFrameHoles)
      .strokeWidth(sw)
      .stroke(c)
      .fill(new Rune.Color (0,0,0,0))

    r.circle(hole, bar.h-frame.hole.yOff, frame.hole.r, gFrameHoles)
      .strokeWidth(sw)
      .stroke(c)
      .fill(new Rune.Color (0,0,0,0))
    
  }

  r.circle(bar.w-frame.off*2, frame.hole.yOff*3, frame.hole.r, gFrameHoles)
    .strokeWidth(sw)
    .stroke(c)
    .fill(new Rune.Color (0,0,0,0))

  r.circle(bar.w+frame.off*10, frame.hole.yOff*3, frame.hole.r, gFrameHoles)
    .strokeWidth(sw)
    .stroke(c)
    .fill(new Rune.Color (0,0,0,0))

  r.circle(bar.w-frame.off*2, bar.h-frame.hole.yOff*3, frame.hole.r, gFrameHoles)
    .strokeWidth(sw)
    .stroke(c)
    .fill(new Rune.Color (0,0,0,0))

  r.circle(bar.w+frame.off*10, bar.h-frame.hole.yOff*3, frame.hole.r, gFrameHoles)
    .strokeWidth(sw)
    .stroke(c)
    .fill(new Rune.Color (0,0,0,0))

}

gScale = (scale) => {
  gBars.scale(scale)
  gBarHoles.scale(scale)
  gFrame.scale(scale)
  gFrameHoles.scale(scale)
}

scalePpi = (ppi) => gScale (ppi / 25.4)

// editing
// drawFrame(style.sw, frame.c)
// drawBars(style.sw, bar.c)
// gScale(2)

// cutting
drawFrame(style.lc, style.cut)
drawBars(style.lc, style.cut)
scalePpi(72)

r.draw()
