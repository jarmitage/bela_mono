let compressor = {
    Threshold: -42.0,  ThresholdMin: -60.0, ThresholdMax: -3.0,    ThresholdStep: 0.1,
    Ratio:      4.0,   RatioMin:      1.0,  RatioMax:      20.0,   RatioStep:     0.1,
    Attack:     250.0, AttackMin:     0.01, AttackMax:     1000.0, AttackStep:    0.01,
    Release:    75.0,  ReleaseMin:    10.0, ReleaseMax:    1000.0, ReleaseStep:   0.01,
    Gain:      -9.0,   GainMin:      -60.0, GainMax:       -0.1,   GainStep:      0.1,
    Mix:        0.5,   MixMin:        0.0,  MixMax:        1.0,    MixStep:       0.01,
    Knee:       0,     KneeMin:       0,    KneeMax:       1.1,    KneeStep:      1,
    Transfer:   0,     TransferMin:   0,    TransferMax:   1.1,    TransferStep:  1,
    Bypass:     0,     BypassMin:     0,    BypassMax:     1.1,    BypassStep:    1
}

let gain = {
    input:  1.0, inputMin:  0.0, inputMax:  5.0, inputStep:  0.1,
    output: 1.0, outputMin: 0.0, outputMax: 5.0, outputStep: 0.1 
}

function updateCompressor(){
    let msg = {
        command: 'updateCompressor',
        args: {
            threshold: compressor.Threshold,
            ratio:     compressor.Ratio,
            attack:    compressor.Attack,
            release:   compressor.Release,
            gain:      compressor.Gain,
            mix:       compressor.Mix,
            knee:      compressor.Knee,
            transfer:  compressor.Transfer,
            bypass:    compressor.Bypass
        }
    }
    Bela.control.send(msg)
    console.log('updateCompressor()\n', msg)
}

function updateIOGain(){
    let msg = {
        command: 'updateInOutGain',
        args: {
            in:  gain.input,
            out: gain.output
        }
    }
    Bela.control.send(msg)
    console.log('updateInOutGain()\n', msg)
}

var guiSketch = new p5(function( sketch ) {

    let canvas_dimensions = [sketch.windowWidth, sketch.windowHeight];

    let mouseDebounceFrames = 10;
    let mouseDebounceCount = 0;

    sketch.setup = function() {
        sketch.createCanvas(canvas_dimensions[0], canvas_dimensions[1]);
        gui = sketch.createGui(this);
        gui.addObject(compressor);
        gui.addObject(gain);
    };

    sketch.draw = function() {
        sketch.background(0);
    };

    sketch.windowResized = function() {
        sketch.resizeCanvas(canvas_dimensions[0], canvas_dimensions[1]);
    };

    sketch.mouseDragged = function(){
        if (++mouseDebounceCount >= mouseDebounceFrames) {
            updateCompressor()
            updateIOGain()
            mouseDebounceCount = 0
        }
    }
    
}, 'gui');
