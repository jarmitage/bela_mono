let biquad = {
    type: 0,     typeMin:  0,   typeMax:  6,     typeStep: 1,
    Fc:   500,   FcMin:    0,   FcMax:    20000, FcStep:   100,
    Q:    0.707, QMin:     0.5, QMax:     0.9,   QStep:    0.005,
    gain: 0.0,   gainMin: -1.0, gainMax:  1.0,   gainStep: 0.05
}

let gain = {
    input:  1.0, inputMin:  0.0, inputMax:  5.0, inputStep:  0.1,
    output: 1.0, outputMin: 0.0, outputMax: 5.0, outputStep: 0.1 
}

function updateBiquad(){
    let msg = {
        command: 'updateBiquad',
        args: {
            type: biquad.type,
            Fc:   biquad.Fc,
            Q:    biquad.Q,
            gain: biquad.gain
        }
    }
    Bela.control.send(msg)
    console.log('updateBiquad()\n', msg)
}

function updateInOutGain(){
    let msg = {
        command: 'updateInOutGain',
        args: {
            in:  gain.input,
            out: gain.output
        }
    }
    Bela.control.send(msg)
    console.log('updateInOutGain()\n', msg)
}

var guiSketch = new p5(function( sketch ) {

    let canvas_dimensions = [sketch.windowWidth, sketch.windowHeight];

    let mouseDebounceFrames = 10;
    let mouseDebounceCount = 0;

    sketch.setup = function() {
        sketch.createCanvas(canvas_dimensions[0], canvas_dimensions[1]);
        gui = sketch.createGui(this);
        gui.addObject(biquad);
        gui.addObject(gain);
    };

    sketch.draw = function() {
        sketch.background(0);
    };

    sketch.windowResized = function() {
        sketch.resizeCanvas(canvas_dimensions[0], canvas_dimensions[1]);
    };

    sketch.mouseDragged = function(){
        if (++mouseDebounceCount >= mouseDebounceFrames) {
            updateBiquad()
            updateInOutGain()
            mouseDebounceCount = 0
        }
    }
    
}, 'gui');
