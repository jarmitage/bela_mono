#include <Bela.h> 
#include <libraries/Gui/Gui.h>
#include <libraries/Scope/Scope.h>
  
// #include "prog/Program.h"
// #include "prog/2019/12/08/2019-12-08_2xAudio.h"
// #include "prog/2019/12/08/2019-12-08_4xAnalog.h"
// #include "prog/2019/12/08/2019-12-08_BelaMidiSetup.h"
// #include "prog/2019/12/08/2019-12-08_SenselFwd.h"
// #include "prog/2019/12/08/2019-12-08_4xAnalog_SenselFwd.h" 
// #include "prog/2019/12/09/2019-12-09_2xAudio_Scope.h"
// #include "prog/2019/12/09/2019-12-09_2xAudio_2xAnalog_Scope.h"
// #include "prog/2019/12/10/2019-12-10_2xAnalog_Scope.h"
// #include "prog/2019/12/10/2019-12-10_EQ3B.h"
// #include "prog/2019/12/10/2019-12-10_EQ3B_2xRes.h" 
// #include "prog/2019/12/11/2019-12-11_4xAnalog_EQ3B.h"
// #include "prog/2019/12/11/2019-12-11_Biquad_Gui_Scope.h"
// #include "prog/2019/12/12/2019-12-12_Gui_sendControl.h"
// #include "prog/2019/12/12/2019-12-12_2xAudio_sendControl.h"
// #include "prog/2019/12/12/2019-12-12_SenselMidi.h"
// #include "prog/2019/12/13/2019-12-13_Compressor.h"
// #include "prog/2019/12/13/2019-12-13_BiquadEQ_Gui_Scope.h"
// #include "prog/2019/12/13/2019-12-13_EQ_Compressor.h"
// #include "prog/2019/12/13/2019-12-13_render_gui_test.h"
// #include "prog/2019/12/13/2019-12-13_1xPiezoAnaEQCompRes.h"
// #include "prog/2019/12/13/2019-12-13_1xPiezoAnaEQRes.h"
// #include "prog/2019/12/14/2019-12-14_4xPiezoAnaEQRes.h"
// #include "prog/2019/12/14/2019-12-14_4xPiezoAnaEQResSensel.h"
// #include "prog/2019/12/17/2019-12-17_4xPiezoAnaEQResSensel.h"
// #include "prog/2019/12/19/2019-12-19_4xPiezoAnaEQResSensel.h"
// #include "prog/2019/12/20/2019-12-20_CuttleNoiseGate.h"
#include "prog/2020/01/2020-01-02_4xPiezoAnaEQResSenselGate.h"
// #include "prog/2020/01/2020-01-07_SvelteTemplate.h"
   
Gui     g; 
Scope   s;
Program p;
          
// No MIDI
// bool setup(BelaContext *context, void *userData){
// 	g.setup(context->projectName);
// 	s.setup(4, context->audioSampleRate);
// 	return p.setup(context, userData, g, s);
// } 

// MIDI
void midiCallback(MidiChannelMessage msg, void* arg){p.onMidi(msg, arg);}
bool setup(BelaContext *context, void *userData){
	g.setup(context->projectName);
	s.setup(4, context->audioSampleRate);
	return p.setup(context, userData, g, s, midiCallback);
}

void render(BelaContext *context, void *userData){p.render(context, userData);}
void cleanup(BelaContext *context, void *userData){p.cleanup(context, userData);}
