// http://cackleberrypines.net/transmogrifox/src/bela/feedback_compressor/feedback_compressor.zip

//Setup analog control input functions
    // kp = (knobs**) malloc(sizeof(knobs*)*N_KNOBS);
    // for(int i = 0; i < N_KNOBS; i++) {
    //     kp[i] = make_knob(kp[i], i, context->audioFrames, context->audioSampleRate, 0.15, 1.0 , 0.01, 0.015);
    // }

// //
// //Control functions related struct and processing
// // 
// typedef struct knobs_t
// {
//     float fs;
//     float ifs;
//     int N;
//     int channel;
    
//     bool active;
    
//     int active_timer;
//     int active_time;
    
//     float *frame;
//     float b0;
//     int scan_time;
//     float dk_thrs;
    
//     float y1, y2;
//     float s0;
//     int scan_timer;
// } knobs;

// knobs* make_knob(knobs* k, int channel, int N, float fs, float scan_time, float active_time, float tau, float thrs)
// {
//     k = (knobs*) malloc(sizeof(knobs));
//     k->frame = (float*) malloc(sizeof(float)*N);
//     k->fs = fs;
//     k->ifs = 1.0/fs;
//     k->N = N;
//     k->channel = channel;
    
//     for(int i = 0; i < N; i++)
//         k->frame[i] = 0.0;
    
//     k->b0 =  expf(-k->ifs/tau);
//     k->scan_time = ((int) (fs*scan_time)) / N;
//     k->active_time = ((int) (fs*active_time) ) / N;
//     k->dk_thrs = thrs;
    
//     k->y1 = k->y2 = 0.0;
//     k->scan_timer = 0;
//     k->active = false;
//     k->active_timer = 0;
    
//     return k;
// }

// //
// //Filter analog inputs
// // Detect changes in position
// // 
// void knob_filter_run(knobs* k)
// {
//     float x = 0.0;
//     //2 first order low-pass filters on analog inputs to smooth knob controls
//     for(int i = 0; i < k->N; i++)
//     {
//         x = k->frame[i];
//         k->y1 = x + (k->y1 - x) * k->b0;
//         x = k->y1;
//         k->y2 = x + (k->y2 - x) * k->b0;       
//     }
    
//     //then check for changes
//     if(++k->scan_timer >= k->scan_time)
//     {
//         k->scan_timer = 0;
//         float dk = fabs(k->y2 - k->s0);
//         if(dk > k->dk_thrs)
//         {
//             k->active_timer = k->active_time;
//             //rt_printf("KNOB dk: %d, %f\n", k->channel, k->y2);
//         }
            
//         k->s0 = k->y2;
//     }
//     if(k->active_timer > 0)
//     {
//         if(k->active == false) 
//             rt_printf("KNOB is ACTIVE: %d, %f\n", k->channel, k->y2);
//         k->active = true;
//         k->active_timer--;
//     }
//     else
//     {
//         if(k->active == true) 
//         {
//             rt_printf("KNOB %d is INACTIVE.  Value: %f\n", k->channel, k->y2);
//         }
//         k->active = false;
//         k->active_timer = 0;
//     }
// }

// void
// knob_destructor(knobs* k)
// {
// 	free(k);
// }

// //Control knobs:
// knobs **kp;