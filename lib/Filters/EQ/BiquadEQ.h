#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>

#include <Bela.h>
#include "libraries/Biquad/Biquad.h"

class BiquadEQ
{
public:
	BiquadEQ(){}
	~BiquadEQ(){}
	
	enum BiquadType {
		BQ_LOWPASS = 0,
		BQ_HIGHPASS,
		BQ_BANDPASS,
		BQ_NOTCH,
		BQ_PEAK,
		BQ_LOWSHELF,
		BQ_HIGHSHELF
	};

	typedef struct _BiquadParams {
		int    type;
		float  Fc;
		float  Q;
		double gain;
	} BiquadParams;

	void setup(std::vector<BiquadParams> params, float sampleRate) {
		sr = sampleRate;

		for (int i = 0; i < params.size(); ++i) {
			Biquad tmp_bq;
			tmp_bq.setup(params[i].Fc, sr, params[i].type, params[i].Q, params[i].gain);
			eq.push_back(tmp_bq);
		}
	
	}

	float process(float in) {
		float out = 0.0;
		for (int i = 0; i < eq.size(); ++i)
			out += eq[i].process(in);
		return out;
	}

	void update(std::vector<BiquadParams> params) {
		updateEQ(params);
	}

	void updateEQ(std::vector<BiquadParams> params) {
		if (params.size() != eq.size()){
			printf("[BiquadEQ] updateEQ(): params.size() != eq.size()\n");
			return;
		}

		for (int i = 0; i < params.size(); ++i)
			updateBiquad(i, params[i].type, params[i].Fc, params[i].Q, params[i].gain);
	}

	void updateBiquad(int index, int _type, float _Fc, float _Q, double _gain) {
		
		int i = index;
		
		int   type = eq[i].getType();
		float Fc   = eq[i].getFc();
		float Q    = eq[i].getQ();
		float gain = eq[i].getPeakGain();

	    if (type != _type) { type = _type; eq[i].setType(type);     }
	    if (Fc   != _Fc)   { Fc   = _Fc;   eq[i].setFc(Fc);         }
	    if (Q    != _Q)    { Q    = _Q;    eq[i].setQ(Q);           }
	    if (gain != _gain) { gain = _gain; eq[i].setPeakGain(gain); }
	    
	}

	void updateFilter(int index, int _type, float _Fc, float _Q, double _gain){
		updateBiquad(index, _type, _Fc, _Q, _gain);
	}

	void printEQ(){
		for (int i = 0; i < eq.size(); ++i)
			printBiquad(i);
	}
	void printBiquad(int index) {
		int i = index;
	    std::string typeS = "";
	    switch (eq[i].getType()) {
			case 0: typeS = "lowpass";   break;
			case 1: typeS = "highpass";  break;
			case 2: typeS = "bandpass";  break;
			case 3: typeS = "notch";     break;
			case 4: typeS = "peak";      break;
			case 5: typeS = "lowshelf";  break;
			case 6: typeS = "highshelf"; break;
	    }
		rt_printf("[BiquadEQ] Index %d, Type %s, Fc %f, Q %f, gain %f\n", i, typeS.c_str(), eq[i].getFc(), eq[i].getQ(), eq[i].getPeakGain());
	}
	void printFilter(int index){printBiquad(index);}

private:
	std::vector<Biquad> eq;
	float sr;
};