// https://www.musicdsp.org/en/latest/Filters/236-3-band-equaliser.html

#ifndef EQ3B_H_
#define EQ3B_H_

#include <math.h>
#include <stdio.h>

class EQ3B {
public:
  EQ3B(){}
  ~EQ3B(){}

  void setup(double sampleRate) {
    es.sr = sampleRate;
    setGains(1.0, 1.0, 1.0);
    setFreqs(defaultLow, defaultHigh);
  }

  void setup(double lowfreq, double highfreq, double sampleRate) {
    es.sr = sampleRate;
    setGains(1.0, 1.0, 1.0);
    setFreqs(lowfreq, highfreq);
  }

  void setup(double lowfreq, double highfreq, double lowGain, double midGain, double highGain, double sampleRate) {
    es.sr = sampleRate;
    setGains(lowGain, midGain, highGain);
    setFreqs(lowfreq, highfreq);
  }

  void setFreqs(double low, double high) {
    es.lf = 2 * sin(M_PI * (low  / es.sr));
    es.hf = 2 * sin(M_PI * (high / es.sr));
  }

  void setGains(double low, double mid, double high) {
    es.lg = low;
    es.mg = mid;
    es.hg = high;
  }

  double process(double sample) {
    double l,m,h; // Low / Mid / High

    // Filter #1 (lowpass)
    es.f1p0  += (es.lf * (sample   - es.f1p0)) + vsa;
    es.f1p1  += (es.lf * (es.f1p0 - es.f1p1));
    es.f1p2  += (es.lf * (es.f1p1 - es.f1p2));
    es.f1p3  += (es.lf * (es.f1p2 - es.f1p3));

    l = es.f1p3;

    // Filter #2 (highpass)
    es.f2p0  += (es.hf * (sample   - es.f2p0)) + vsa;
    es.f2p1  += (es.hf * (es.f2p0 - es.f2p1));
    es.f2p2  += (es.hf * (es.f2p1 - es.f2p2));
    es.f2p3  += (es.hf * (es.f2p2 - es.f2p3));

    h = es.sdm3 - es.f2p3;

    // Calculate midrange (signal - (low + high))
    m = es.sdm3 - (h + l);

    // Scale, Combine and store
    l *= es.lg;
    m *= es.mg;
    h *= es.hg;

    // Shuffle history buffer
    es.sdm3 = es.sdm2;
    es.sdm2 = es.sdm1;
    es.sdm1 = sample;

    return(l + m + h);
  }
private:
  const double vsa = (1.0 / 4294967295.0); // Very small amount (Denormal Fix)

  typedef struct _EQ3BSTATE {
    // Filter #1 (Low band)
    double lf;   // Frequency
    double f1p0; // Poles ...
    double f1p1;
    double f1p2;
    double f1p3;

    // Filter #2 (High band)
    double hf;   // Frequency
    double f2p0; // Poles ...
    double f2p1;
    double f2p2;
    double f2p3;

    // Sample history buffer
    double sdm1; // Sample data minus 1
    double sdm2; //                   2
    double sdm3; //                   3

    // Gain Controls
    double lg; // low  gain
    double mg; // mid  gain
    double hg; // high gain

    double sr; // sample rate
  } EQ3BSTATE;

  EQ3BSTATE es = {};

  double defaultLow = 500;
  double defaultHigh = 5000;
};

#endif // #ifndef EQ3B_H_
