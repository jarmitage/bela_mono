#include <libraries/Midi/Midi.h>

#include "lib/Devices/Sampler/SampleTriggerDigital.h"
#include "lib/Utils/Pitch.h"

class SenselMidiGuiSampler
{
public:
	SenselMidiGuiSampler(){}
	~SenselMidiGuiSampler(){}
	void setup(Gui &_gui, void (*callback)(MidiChannelMessage, void*), std::string _sample, int _velocity){
		gui = &_gui;

		midi.readFrom(port);
		midi.writeTo(port);
		midi.enableParser(true);
		midi.setParserCallback(callback, (void*) port);
		rt_printf("[SenselMidiGui] setupMIDI() setup on port %s\n", port);

		sample = _sample;
		velocity = _velocity;
		sampler.setup(sample);
	}
	float render(){
		return sampler.render(0);
	}
	void onMidi(MidiChannelMessage msg, void* arg){
		// msg.prettyPrint();
		if(msg.getType() == kmmNoteOn){
			// TODO: only play sample if the note is attached to a function
			sampler.trigger(velocity);
			noteOnToGui(msg);
		}
	}
	void noteOnToGui(MidiChannelMessage msg){
	    if (gui->isConnected()) {
			JSONObject root;
			root[L"sensel"]  = new JSONValue (msg.getDataByte(0));
			JSONValue* value = new JSONValue (root);
			gui->sendControl(value);
			rt_printf("[SenselMidiGui] Forward NoteOn: %d\n", msg.getDataByte(0));
	    } else {
			rt_printf("[SenselMidiGui] GUI not connected\n");
	    }
	}
private:
	Midi midi;
	Gui *gui;
	SampleTriggerDigital sampler;
	// Pitch p; // vector?
	std::string sample = "data/samples/ui/ui_01.wav";
	int velocity = 127;
	const char* port = "hw:1,0,0";


};