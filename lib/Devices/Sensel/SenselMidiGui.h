#include <libraries/Midi/Midi.h>

#include "lib/Utils/Pitch.h"

class SenselMidiGui
{
public:
	SenselMidiGui(){}
	~SenselMidiGui(){}
	void setup(Gui &_gui, void (*callback)(MidiChannelMessage, void*)){
		gui = &_gui;
		midi.readFrom(port);
		midi.writeTo(port);
		midi.enableParser(true);
		midi.setParserCallback(callback, (void*) port);
		rt_printf("[SenselMidiGui] setupMIDI() setup on port %s\n", port);
	}
	void onMidi(MidiChannelMessage msg, void* arg){
		// msg.prettyPrint();
		if(msg.getType() == kmmNoteOn)
			noteOnToGui(msg);
	}
	void noteOnToGui(MidiChannelMessage msg){
	    if (gui->isConnected()) {
			JSONObject root;
			root[L"note"] = new JSONValue (msg.getDataByte(0));
			JSONValue* value = new JSONValue (root);
			gui->sendControl(value);
			rt_printf("[SenselMidiGui] send NoteOn: %d\n", msg.getDataByte(0));
	    } else {
			rt_printf("[SenselMidiGui] Gui not connected\n");
	    }
	}
private:
	Midi midi;
	Pitch p; // vector?
	const char* port = "hw:1,0,0";
	Gui *gui;
};