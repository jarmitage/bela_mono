#include <libraries/Midi/Midi.h>

#include "lib/Utils/Pitch.h"

class SenselMidi
{
public:
	SenselMidi(){}
	~SenselMidi(){}
	void setup(void (*callback)(MidiChannelMessage, void*)){
		midi.readFrom(port);
		midi.writeTo(port);
		midi.enableParser(true);
		midi.setParserCallback(callback, (void*) port);
		rt_printf("[render] setupMIDI() setup on port %s\n", port);
	}
	void onMidi(MidiChannelMessage msg, void* arg){
		msg.prettyPrint();
		if(msg.getType() == kmmNoteOn)
			handleNoteOn(msg);
	}
	void handleNoteOn(MidiChannelMessage msg){
		// gui.sendBuffer(0, msg.getDataByte(0));
	}
private:
	Midi midi;
	Pitch p; // vector?
	const char* port = "hw:1,0,0";
};