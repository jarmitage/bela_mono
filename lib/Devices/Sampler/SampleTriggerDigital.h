#include "SampleLoader.h"
#include "SampleData.h"

#define NUM_CHANNELS 1

// TODO: Sampler should hold vector of samples

typedef struct _SampleTriggerData {
	string filename = "sample.wav";

	SampleData buf[NUM_CHANNELS];
	int readPtr;	// Position of last read sample from file

	int triggered = 0;
	int velocity = 127;

	// debounce not necessary?
	int debounce = 250;
	int debounceCount = 0;
} SampleTriggerData;


class SampleTriggerDigital
{
public:
	SampleTriggerDigital(){}
	~SampleTriggerDigital(){}
	void setup() { loadSample(); }
	void setup(string _filename){
		sTrig.filename = _filename;
		loadSample();
	}
	void trigger() {
		triggerSample(127);
	}
	void trigger(int _velocity) {
		triggerSample(_velocity);
	}
	float render(int channel){
		float out = 0;

		// If triggered read each sample
	    if(sTrig.readPtr != -1)
	    	out = sTrig.buf[channel%NUM_CHANNELS].samples[sTrig.readPtr++];

	    // Reset ptr
    	if(sTrig.readPtr >= sTrig.buf[channel%NUM_CHANNELS].sampleLen) {
    		sTrig.triggered = 0;
	    	sTrig.readPtr = -1;
    	}

		return out * (sTrig.velocity/127.0);
	}
	void cleanup(){
		for(int ch = 0; ch < NUM_CHANNELS; ch++)
	    	delete[] sTrig.buf[ch].samples;
	}
private:
	SampleTriggerData sTrig = {};

	void loadSample() {

		for(int ch = 0; ch < NUM_CHANNELS; ch++) {
        	sTrig.buf[ch].sampleLen = getNumFrames(sTrig.filename);
    		sTrig.buf[ch].samples = new float[sTrig.buf[ch].sampleLen];
        	getSamples(sTrig.filename, sTrig.buf[ch].samples, ch, 0, sTrig.buf[ch].sampleLen);
		}

		sTrig.readPtr = -1;
	}

	void triggerSample(int _velocity) {
		// if(!sTrig.triggered && ++sTrig.debounceCount > sTrig.debounce) {
    	// if(!sTrig.triggered) {
    	sTrig.triggered = 1; 
    	sTrig.readPtr   = 0;
		sTrig.velocity  = _velocity;
	    	// sTrig.debounceCount = 0;
	  	// }
	}

};
