#include "SampleLoader.h"
#include "SampleData.h"

#define NUM_CHANNELS 1

typedef struct _SampleTriggerData {
	string filename = "sample.wav";

	SampleData buf[NUM_CHANNELS];
	int readPtr;	// Position of last read sample from file

	// DC OFFSET FILTER
	float prevReadingDCOffset = 0;
	float prevInputReading = 0;
	float readingDCOffset = 0;
	float R = 0.99;//1 - (250/44100);

	// For onset detection
	float peakValue = 0;
	float thresholdToTrigger = 0.001;
	float amountBelowPeak = 0.001;
	float rolloffRate = 0.00005;
	int triggered = 0;    

	// debounce
	int debounce = 250;
	int debounceCount = 0;
} SampleTriggerData;


class SampleTriggerAnalog
{
public:
	SampleTriggerAnalog(){}
	~SampleTriggerAnalog(){}
	void setup() { loadSample(); }
	void setup(string _filename){
		sTrig.filename = _filename;
		loadSample();
	}
	void process(float input) {
		dcOffset(input);
		fullWaveRectify();
		detectOnset();
		triggerSample();
	}
	float render(int channel){
		float out = 0;
		// If triggered read each sample
	    if(sTrig.readPtr != -1)
	    	out = sTrig.buf[channel%NUM_CHANNELS].samples[sTrig.readPtr++];

	    // Reset ptr
    	if(sTrig.readPtr >= sTrig.buf[channel%NUM_CHANNELS].sampleLen)
	    	sTrig.readPtr = -1;

		return out;
	}
	void cleanup(){
		for(int ch = 0; ch < NUM_CHANNELS; ch++)
	    	delete[] sTrig.buf[ch].samples;
	}
private:
	SampleTriggerData sTrig = {};

	void loadSample(){

		for(int ch = 0; ch < NUM_CHANNELS; ch++) {
        	sTrig.buf[ch].sampleLen = getNumFrames(sTrig.filename);
    		sTrig.buf[ch].samples = new float[sTrig.buf[ch].sampleLen];
        	getSamples(sTrig.filename, sTrig.buf[ch].samples, ch, 0, sTrig.buf[ch].sampleLen);
		}

		sTrig.readPtr = -1;
	}

	void dcOffset(float input) {
		// Re-centre around 0
		// DC Offset Filter    y[n] = x[n] - x[n-1] + R * y[n-1]
		sTrig.readingDCOffset     = input - sTrig.prevInputReading + (sTrig.R * sTrig.prevReadingDCOffset);
		sTrig.prevInputReading    = input;
		sTrig.prevReadingDCOffset = sTrig.readingDCOffset;
	}

	void fullWaveRectify() {
	    if(sTrig.readingDCOffset < 0) 
	    	sTrig.readingDCOffset *= -1.0f;
	}

	void detectOnset() {
		// Onset Detection
		// Record the highest incoming sample
		// But have the peak value decay over time
		// so we can catch the next peak later
		if(sTrig.readingDCOffset >= sTrig.peakValue) { 
			sTrig.peakValue = sTrig.readingDCOffset;
			sTrig.triggered = 0;
		} else if (sTrig.peakValue >= sTrig.rolloffRate) {
	    	sTrig.peakValue -= sTrig.rolloffRate;
		}
	}

	void triggerSample(){
		// Indicate that we've triggered and wait 
		// for the next peak before triggering again.
		// Start sample playback
		if(sTrig.readingDCOffset < sTrig.peakValue - sTrig.amountBelowPeak && 
		   sTrig.peakValue >= sTrig.thresholdToTrigger && 
		   !sTrig.triggered &&
		   ++sTrig.debounceCount > sTrig.debounce)
		{
	    	rt_printf("[SampleTrigger.h] triggerSample() peakValue: %f\n", sTrig.peakValue);
	    	sTrig.triggered = 1; 
	    	sTrig.readPtr = 0;
	    	sTrig.debounceCount = 0;
	  	}
	}

};
