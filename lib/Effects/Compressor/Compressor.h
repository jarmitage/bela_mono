/*
 * Compressor.h
 *
 * Port of http://cackleberrypines.net/transmogrifox/src/bela/feedback_compressor/feedback_compressor.zip
 * 
 *    Parallel Control                 Feedback Sidechain Works on Envelope
 *   ^^^^^^^^^^^^^^^^^                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *    xn--O--->[Peak Detector]-------->{X}----------------------O--->yn (envelope)
 *        |                             |                       |
 *       {X}<----Control Gain------O----+                [ATTACK/RELEASE]<------+
 *        |                        |                     [ (Ballistics) ]       |
 *       {X}<--Makeup Gain    |->[OR]<-{+}<--1                  |               |
 *        |                   {e^-KX}   |-                      |               |
 *       {X}<--Output Level   [!lin?]<-{X}<--K (ratio)         {+}_<--t         |
 *        |                             |                       |               |
 *        |                             |_______________________|               |
 *        |                                                                     |
 *        O---->[Output over range detection}-----------------------------------+
 *        |
 *        +--[CLIP]-->yo {Final Output}
 * 
 *   Not shown:  Knee Processing, Wet/Dry Mix
 * 
*/

#ifndef COMPRESSOR_H_
#define COMPRESSOR_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <math_neon.h> // change to "libraries/..." ?
#include <vector>

class Compressor
{
public:
	Compressor();
	~Compressor();

	void setup(float audioSampleRate, float audioFrames);
	std::vector<float> process(std::vector<float> block);

	// Convenience function for updating everything at once
	void updateSettings(float t, float r, float a, float rl, float g, float w, bool k, bool f, bool b);

	void setThreshold(float _threshold); // Units of dB: -infinity to 0 dB
	// Compression ratio
	// This is not a ratio in the traditional sense if linmode is set 'true'.
	// In this mode the amount of perceived compression roughly correlates to typical feed-forward
	// compression ratio. If input level is 0 dB, then output is
	// -threshold + threshold/ratio, but in between is not log-linear
	// This mode is most useful with lower threshold and lower ratio settings
	// as it has the effect of a continuously increasing ratio with signal level--
	// threshold behaves like a knee radius making for a very soft-knee compression.
	// When linmode == 'false' then the exponential feedback gain is applied and 
	// the ratio represents the traditional log-linear transfer function
	// input unitless: 1 to 20
	void setRatio(float _ratio);     
	void setAttack(float _attack);   // 1 ms to 1000 ms
	void setRelease(float _release); // 10 ms to 1000 ms
	void setOutputGain(float _gain); // -db_dynrange (-60dB) to 20 dB
	// Parallel compression option (wet/dry mix).  Wet range: 0.0 to 1.0
	// Can help preserve some of the transients while providing sustain
	void setMix(float _wet);
	void setKnee(bool _soft); // soft (true) or hard (false)
	// Either linear feedback mode (traditional compression curve) (true) or 
	// inverse exponential feedback yielding a traditional log-linear transfer function (false)
	void setTransferFunction(bool _linear);
	// if bp = true, it is forced to bypass mode and function returns true.
	// if bp = false, bypass state is toggled and new state is returned
	void setBypass(bool _bypass);

	float getThreshold()        {return state.threshold_db;}
	float getRatio()            {return state.ratio;}
	float getAttack()           {return state.t_attack  *= 1000.0;}
	float getRelease()          {return state.t_release *= 1000.0;}
	float getOutputGain()       {return state.out_gain;}
	float getMixWet()           {return state.wet;}
	float getMixDry()           {return state.dry;}
	bool  getKnee()             {return state.soft_knee;}
	bool  getTransferFunction() {return state.linmode;}
	bool  getBypass()           {return state.bypass;}

	void printSettings();

private:

	void update();

	// Process sub-functions, in execution order
	bool  processBypass();
	float processDetectPeak(int i, float yn, float _in);
	float processLimitPeak(float yn);
	void  processBallistics(float yn);
	float processSoftKnee();
	void  processGainReduction(float yk);
	float processGain(float _in);
	float processLimiting(float _in);

	typedef struct _CompressorState {
	    //System Parameters
	    float fs;  // Sample rate
	    float ifs; // Inverse sample rate
	    int N;     // Processing block size

	    //User settings
	    bool bypass, reset;
	    bool linmode;
	    float threshold_db;
	    float ratio;
	    float t_attack;
	    float t_release;
	    float out_gain;

	    //Internal variables
	    float atk, atk0, rls;        // attack and release coefficients
	    float lmt_atk;               // Attack time when limiting
	    float lmt_thrsh;             // Sets when to start speeding up attack time
	    float k, tk, tkp1, mk, tpik; // Related to compression ratio
	    float t;                     // Threshold
	    float g;                     // Static gain (makeup_gain * out_gain)
	    float db_dynrange;           // how much gain reduction will be allowed
	    float dynrange;
	    float makeup_gain;
	    float wet, dry;

	    //Peak detector
	    float pkrls;
	    float pk_hold_time;
	    
	    //knee
	    bool soft_knee;
	    float knt, hknt, iknt;

	    //state variables
	    float gain;
	    float y1;
	    float pk;
	    float rr[4];
	    int rr_cyc;
	    int pk_timer;

	} CompressorState;

	CompressorState state = {};

};

#endif /* COMPRESSOR_H_ */
