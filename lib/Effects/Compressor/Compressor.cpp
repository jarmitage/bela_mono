/*
 * Compressor.cpp
 *
 * Port of http://cackleberrypines.net/transmogrifox/src/bela/feedback_compressor/feedback_compressor.zip
 */

#include "Compressor.h"

Compressor::Compressor(){}
Compressor::~Compressor(){}

void Compressor::setup(float audioSampleRate, float audioFrames) {

    state.fs  = audioSampleRate;
    state.ifs = 1.0 / audioSampleRate;
    state.N   = audioFrames;

	state.linmode      = false;
    state.threshold_db = -36.0;
    state.ratio        = 8.0;
    state.db_dynrange  = 60.0;
    setOutputGain(-6.0);
    state.t_attack     = 50.0e-3;
    state.t_release    = 75.0e-3;
    state.wet          = 1.0;
    state.dry          = 0.0;

    state.atk          = expf(-state.ifs / state.t_attack);
    state.atk0         = state.atk;
    state.lmt_atk      = expf(-state.ifs / 0.001);
    state.rls          = expf(-state.ifs / state.t_release);
    state.pkrls        = expf(-state.ifs / 0.008);
    state.pk_hold_time = lrintf(0.0025 * state.fs);

    state.t        = powf(10.0, state.threshold_db / 20.0);
    state.k        = powf(10.0, state.ratio        / 20.0);
    update();
    state.dynrange = powf(10.0, -state.db_dynrange / 20.0);

    state.gain     = 1.0;
    state.y1       = 0.0;
    state.pk       = 0.0;
    state.pk_timer = 0;
    state.rr_cyc   = 0;
    for(int n = 0; n < 4; n++) state.rr[n] = 0.0;

    state.bypass    = true;
    state.reset     = true; // everything has been initialized
    state.soft_knee = true;
}

std::vector<float> Compressor::process(std::vector<float> block) {
	if (processBypass()) return block;

    float yn;

    for (int i = 0; i < state.N; i++) {
        yn = processDetectPeak(i, yn, block[i]);
        yn = processLimitPeak(yn);
		processBallistics(yn);
		float yk = processSoftKnee();
		processGainReduction(yk);
		block[i] = processGain(block[i]);
        block[i] = processLimiting(block[i]);
    }

    return block;
}

bool Compressor::processBypass() {
	if(state.bypass) {
        if(!state.reset) {
            state.gain     = 1.0;
            state.y1       = 0.0;
            state.pk       = 0.0;
            state.pk_timer = 0;
            state.atk0     = state.atk;
            state.reset    = true;
        }
        return true;
    }
    return false;
}

float Compressor::processDetectPeak(int i, float yn, float _in) {
    //  This is a "round-robin" style peak detector.

    //  General strategy inspired by Harry Bissel:
    //   http://www.edn.com/design/analog/4344656/Envelope-follower-combines-fast-response-low-ripple
    //   http://m.eet.com/media/1143121/18263-figure_1.pdf

    //  A circular buffer (fbc-rr[]) is used to store
    //  peak values and it is clocked every pk_hold_time.
    //  The oldest peak value is reset and the maximum
    //  of the circular buffer is used to represent the
    //  envelope.

    //  The output is a square-stepped track & hold
    //  signal that closely follows the signal envelope.
    //  Finally the rapid edges must be smoothed, so
    //  a last stage is added to filter the peak detector.
    //  The end result is a very smooth approximation of the
    //  envelope.

    //  The key is matching the round-robin cycling rate
    //  to the expected type of signal and desired response times.
    //  For guitar and bass it should be set to make a full cycle at about
    //  50 Hz, and hold time is (1/2)*(1/4) of that period.
    //  Bass low E is around 38 Hz, so may want to go slower for bass
    
    //  If response ends up being too fast, but ballistics filtering
    //  is expected to make up the difference on that front

    yn = fabs(_in);
	if (yn > state.rr[state.rr_cyc])
		state.rr[state.rr_cyc] = yn;

    //Cycle Timer
    if (state.pk_timer < state.pk_hold_time) {
        state.pk_timer++;
    } else {
        if (++state.rr_cyc >= 4) state.rr_cyc = 0;
    	state.pk_timer         = 0;
    	state.rr[state.rr_cyc] = 0.0;
    }

    //Final peak value selection circuit
    float tmpk = 0.0;
    for(int n = 0; n < 4; n++)
    	if(state.rr[n] > tmpk) tmpk = state.rr[n];

    //Smooth the stair-steps
    state.pk = tmpk + (state.pk - tmpk) * state.pkrls;

    return yn;
}

float Compressor::processLimitPeak(float yn) {
	// last computed gain on new peak detector value is used
    // to compare against the threshold
    // Apply limit to error signal.
    // Otherwise the excessive overdrive pushes attack time to almost nil
    yn = state.gain * state.pk;
    if (state.linmode) {
             if (yn < state.t)    yn = state.t;
        else if (yn > state.tpik) yn = state.tpik;
    } else {
             if (yn < state.t) yn = state.t;
        else if (yn > 1.0) yn = 1.0;
    }

    return yn;
}

void Compressor::processBallistics(float yn) {
	// Attack/Release on sidechain envelope
    if (yn > state.y1) state.y1 = yn + (state.y1 - yn) * state.atk0; // Attack
    else               state.y1 = yn + (state.y1 - yn) * state.rls;  // Release
}

float Compressor::processSoftKnee() {
    float sk = state.y1 - state.t;
    float yk = 0.0;
    float gk = 0.0;

    if (state.soft_knee) {
    	if (sk < state.hknt) {
    		gk = 1.0 - (state.knt - sk) * state.knt;
    	}
    	else {
    		gk = 1.0;
    		yk = -0.25 * state.knt;
    	}
    	yk += sk * gk;
    	yk += state.t;
    } else{
    	yk = state.y1;
    }

    return yk;
}

void Compressor::processGainReduction(float yk) {
	// Compute gain reduction
    // This relationship is refactored to a more computationally
    // efficient form from a block diagram of the feedback
    // compressor ( gain = 1 - k*(y - t) ).
    // or to do log-linear curve, 
    // gain = e^(k*(t-y))
    if (state.linmode) state.gain = yk * state.mk + state.tkp1;
	else               state.gain = expf_neon(state.tk + yk * state.mk); // linear in log domain
	
         if (state.gain < state.dynrange) state.gain = state.dynrange;
    else if (state.gain > 1.0)            state.gain = 1.0; //paranoia
}

float Compressor::processGain(float _in) {
	// Apply gain (this is the final output)
    // Processing block[i] outside of the feedback loop
    // is a bit of a book-keeping trick.  It helped prevent
    // accidential bug injection.
    // The feedback loop works around the peak detector,
    // which is directly proportional to the input block[i].
    // The final output behavior is equivalent to an analog circuit
    // implementation of a feedback compressor.
    return (state.wet * state.g * state.gain * _in) + (state.dry * _in);
}

float Compressor::processLimiting(float _in) {
	// Either:
	// 1. Increase attack time for fast limiting if over 1.0,
	// 2. Gradually decrease attack when X dB (lmt_thrsh) above out_gain, or
	// 3. Apply normal attack time
	float tmp = fabs(_in) - state.lmt_thrsh;
	     if (tmp > 1.0) state.atk0 = state.lmt_atk;
	else if (tmp > 0.0) state.atk0 = tmp * state.lmt_atk + (1.0 - tmp) * state.atk;
	else                state.atk0 = state.atk;

	// Hard clip what remains outside of limit
	if (_in >  1.0) _in =  1.0;
	if (_in < -1.0) _in = -1.0;

	return _in;
}

void Compressor::update() {
	
	if (state.soft_knee) {
		state.t    = powf(10.0, state.threshold_db / 20.0);
	    state.knt  = state.t;
	    state.hknt = 0.5 * state.knt;
	    state.iknt = 1.0 / state.knt;
	    state.t    = powf(10.0, (state.threshold_db - 3.0) / 20.0);
	} else {
		state.t    = powf(10.0, state.threshold_db / 20.0);
	}

    // Target makeup gain needed for given threshold and ratio setting
    float m = powf(10.0, (state.threshold_db / state.ratio - state.threshold_db) / 20.0);
    state.makeup_gain = m;
    // Feedback ratio dependent upon both threshold and ratio
    state.k = (m - 1.0) / (1.0 - m * state.t);
    if (state.linmode == false) state.k *= log(state.k);
    state.mk   = -state.k;
    state.tk   =  state.t  * state.k;
    state.tkp1 =  state.tk + 1.0;
    state.tpik =  state.t  + 1.414 / state.k;
    state.g    =  state.out_gain * m; // makeup gain
}

void Compressor::updateSettings(float t, float r, float a, float rl, float g, float w, bool k, bool f, bool b) {
    // TODO: Constrain inputs e.g.
    // compressor.setThreshold  (map(t,  0.0, 1.0, -60.0, -3.0));
    // compressor.setRatio      (map(r,  0.0, 1.0,  1.0,   20.0));
    // compressor.setAttack     (map(a,  0.0, 1.0,  1.0,   1000.0));
    // compressor.setRelease    (map(rl, 0.0, 1.0,  10.0,  1000.0));
    // compressor.setOutputGain (map(g,  0.0, 1.0, -60.0,  0.0));
    // compressor.setMix        (map(w,  0.0, 1.0,  0.125, 1.0));

    // TODO: not sure get and set are related to the same thing

    if (getThreshold()  != t)  { setThreshold(t);  }
    if (getRatio()      != r)  { setRatio(r);      }
    if (getAttack()     != a)  { setAttack(a);     }
    if (getRelease()    != rl) { setRelease(rl);   }
    if (getOutputGain() != g)  { setOutputGain(g); }
    if (getMixWet()     != w)  { setMix(w);        }
    
    if (getKnee()             != k) { setKnee(k);             }
    if (getTransferFunction() != f) { setTransferFunction(f); }
    if (getBypass()           != b) { setBypass(b);           }

    update();

}

void Compressor::setThreshold(float _threshold) {
	float t = _threshold; // Units of dB: -infinity to 0 dB

    if (t > 0.0) t = 0.0;
    state.threshold_db = t;

}

void Compressor::setRatio(float _ratio) {
	float r = _ratio; // input unitless: 1 to 20
    
         if (r < 1.0)  r = 1.0;
    else if (r > 20.0) r = 20.0;
    
    state.ratio = r;

}

void Compressor::setAttack(float _attack) {
    float a = _attack; // 1 ms to 1000 ms

         if (a < 0.1)    a = 0.1;    // needs to be some rate limiting for control loop stability
    else if (a > 1000.0) a = 1000.0; // >1s attack not useful and likely a bug.

    a *= 0.001; // convert to units of seconds
    state.t_attack = a;
    state.atk      = expf(-state.ifs / state.t_attack);
    state.atk0     = state.atk;
}

void Compressor::setRelease(float _release) {
	float r = _release; // 10 ms to 1000 ms

         if (r < 10.0)   r = 10.0;   // less than this is probably not useful
    else if (r > 1000.0) r = 1000.0; // more is probably not useful and likely a bug.

    r *= 0.001;  // convert to units of seconds
    state.t_release = r;
    state.rls = expf(-state.ifs / (state.t_release));
}

void Compressor::setOutputGain(float _gain) {
	float g = _gain; // -db_dynrange (-60dB) to 20 dB

         if (g < -state.db_dynrange) g = -state.db_dynrange;
    else if (g > 20.0) g = 20.0;

    state.out_gain  = powf(10.0, g / 20.0);
    state.lmt_thrsh = powf(10.0, (g + 6.0) / 20.0);

    if (state.lmt_thrsh > 1.0) state.lmt_thrsh = 1.0;

    state.g = state.out_gain * state.makeup_gain;
}

void Compressor::setMix(float _wet) {
	// Parallel compression option (wet/dry mix).  Wet range: 0.0 to 1.0
	// Can help preserve some of the transients while providing sustain
	float w = _wet;
	     if (w > 1.0) w = 1.0;
	else if (w < 0.0) w = 0.0;
	
	state.wet = w;
	state.dry = 1.0 - w;
}

void Compressor::setKnee(bool _soft) {
	state.soft_knee = _soft; // soft (true) or hard (false)
}

void Compressor::setTransferFunction(bool _linear) {
	// Either linear feedback mode (traditional compression curve) (true) or 
	// inverse exponential feedback yielding a traditional log-linear transfer function (false)
	state.linmode = _linear;
}

void Compressor::setBypass(bool _bypass) {

	if(!_bypass) {
		if(state.bypass) {
			state.bypass = false;
		} else {
            state.bypass = true;
            state.reset = false;
        }
	} else {
		state.bypass = true;
        state.reset = false;
	}
}

void Compressor::printSettings(){
    float t = getThreshold();
    float r = getRatio();
    float a = getAttack();
    float rl = getRelease();
    float g = getOutputGain();
    float w = getMixWet();
    bool  k = getKnee();
    bool  f = getTransferFunction();
    bool  b = getBypass();

    printf("[Compressor] settings. Thresh: %.2f, Ratio: %.2f, Att: %.2f, Rel: %.2f, Gain: %.2f, MixWet: %.2f, Knee: %d, TFunc: %d, Bypass: %d\n", t, r, a, rl, g, w, k, f, b);
}
