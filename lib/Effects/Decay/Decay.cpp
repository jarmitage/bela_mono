/*
 * Decay.h
 */

#include "Decay.h"

Decay::Decay(){}
Decay::~Decay(){}

void Decay::setup (int _delay, float _decay) {

  delay = _delay;
  decay = _decay;

}

float Decay::operator() (float in) {

  updatePointer();

  dry = in;
  wet = 0.0f;

  wet  = dry;
  wet += buffer[indexAtOffset (delay)];
  wet *= decay;

  buffer[bufferPtr] = wet;

  return dry + buffer[indexAtOffset (delay)];

}

int Decay::indexAtOffset (int offset) {
  return (bufferPtr - offset + bufferSize) % bufferSize;
}

void Decay::updatePointer() {
  if (++bufferPtr >= bufferSize) bufferPtr = 0;
}

void Decay::setDelay (int _delay)   { delay = _delay; }
void Decay::setDecay (float _decay) { decay = _decay; }
int Decay::getDelay()   { return delay; }
float Decay::getDecay() { return decay; }
