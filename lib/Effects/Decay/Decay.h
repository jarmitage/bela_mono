/*
 * Decay.h
 */

#ifndef Decay_H_
#define Decay_H_

class Decay {
public:
  Decay();
  ~Decay();
  void setup (int _delay, float _decay);
  
  void setDelay (int _delay);
  void setDecay (float _decay);
  int getDelay();
  float getDecay();

  float operator() (float in);

private:
  void updatePointer();
  int indexAtOffset (int offset);

  // Make this 44100 * ((int)(delay / 44100))
  float buffer [44100];
  int bufferSize = 44100;
  int bufferPtr;

  float decay;
  int delay;

  float dry;
  float wet;

  // Interpolation...
  // int decayPreDelay = 500;
  // int decayPreDelayPrev = 0;
  // int decayPreDelayPrev2 = 0;

};

#endif /* Decay_H_ */
