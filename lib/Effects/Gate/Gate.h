#include "lib/Effects/RMS.h"

class Gate
{
public:
	Gate(){}
	~Gate(){}
	void setup(float _threshold, int _windowSize){ 
		threshold = _threshold;
		rms.setup(_windowSize);
	}
	int process(float in){
		// 0 = closed, 1 = open
		state = (threshold > rms.process(in)) ? 0 : 1;
		return state;
	}
	void setThreshold(float _threshold){threshold = _threshold;}
	float getThreshold(){return threshold;}
	float getRms(){return rms.get();}
	int getState(){return state;}
private:
	RMS rms;
	float threshold;
	int windowSize;
	int state;
};
