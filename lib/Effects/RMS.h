//  Created by punkass on 07/09/2017.
//  Copyright (c) 2017 punkass. All rights reserved.
// 
//  Modified by jarmitage on 20/12/2019

#include "math_neon.h"
#include "lib/Utils/CircularBuffer.h"

class RMS {
public:
    RMS(){}
    ~RMS(){}
    
    void setup(int windowSize){
        mWindowSize = windowSize;
        mWindowBuffer = new CircularBuffer<float>(mWindowSize, 0.0f); // allocate the buffer.
        mCurrentRms = 0.0f;
        mCurrentMs = 0.0f;
    }

    float process(float inSample){
        float toAdd = (inSample*inSample)/mWindowSize;  //get the individual sample MS
        mCurrentMs = mCurrentMs + toAdd;              //add the new sample
        if (mWindowBuffer->isFull()){                   //remove the sample falling out of the window if full
            float toRemove = mWindowBuffer->pop_front();
            mCurrentMs = mCurrentMs - toRemove;
        }
        mWindowBuffer->push_back(toAdd);
        mCurrentRms = sqrtf_neon(mCurrentMs);

        return mCurrentRms;
    }

    float get(void){ return mCurrentRms; }
    void  clear(void) {
        mCurrentRms = 0.0f;
        mCurrentMs = 0.0f;
        mWindowBuffer->clear();
    }
    void cleanup(){delete mWindowBuffer;}

private:
    CircularBuffer<float>* mWindowBuffer;
    int mWindowSize;

    float mCurrentRms; //float for now
    float mCurrentMs;  //float for now  (because sqrt is costly.)

};
