/*
 * Noise.h
 */

// Adapted from
// https://github.com/BelaPlatform/Bela/blob/ea5b169e0a1a5ec35421071ab42f0c0cfbfca540/examples/06-Sensors/LDR/render.cpp#L79

#ifndef Noise_H_
#define Noise_H_

#include <stdlib.h>

template <class T>
T noise() {
  return 0.1f * ( rand() / (float)RAND_MAX * 2.f - 1.f );
}

#endif /* Noise_H_ */
