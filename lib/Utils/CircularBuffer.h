//
//  CircularSensor.h
//  UnitTest
//
//  Created by punkass on 21/02/2017.
//  Copyright (c) 2017 punkass. All rights reserved.
//

#ifndef CircularBuffer_h
#define CircularBuffer_h

/***** Circular.h *****/

#include <assert.h>
#include <vector>
#include <utility> //for std::move

//Convenient Circular Buffer utilizing the push_back and pop_front style interface that should take any type but has only been designed and tested with std containers.  For containers, must provide a useful

//DOES NOT CURRENTLY INCLUDE ANY ERROR HANDLING

//One question I have on this is whether I end up with allocations during pop_front.  I'm returning the reference which makes me think maybe no?  Though there is one version that definitely will request allocation.  Check which one gets called maybe?  Also, if I have returned the reference but then update the size would it be possible to overwrite the same location giving me, essentially an invalid value at the referenced location?
template<class Type>

class CircularBuffer {
    public :
    
    CircularBuffer(const int inSize, const Type inNullVal) : mCapacity(inSize), mNullVal(inNullVal) {
        mInternalBuffer.assign(inSize, inNullVal);  //guess I can't  have an array of standard vectors?
        mRead = 0;
        mWrite = 0;
        mSize = 0;
        mOverwriteOccurred_Flag = false;
    };
    
    CircularBuffer(const int inSize) : mCapacity(inSize), mNullVal(0) { //if it has a simple Null use this
        mInternalBuffer.assign(inSize, mNullVal); // = new Type[inSize]();
        mRead = 0;
        mWrite = 0;
        mSize = 0;
        mOverwriteOccurred_Flag = false;
    };
    
    ~CircularBuffer() {
    };

    //This will overwrite data, but triggers a flag saying so.
    void push_back(const Type inSample) {
        mInternalBuffer[mWrite] = inSample; //do write.  Note this is a copy assignment so the whole "inSample" is copied
        if (mSize == mCapacity){            // advance pointer and check status
            mOverwriteOccurred_Flag = true;
            if (++mRead >= mCapacity){
                mRead = 0;
            }
            mWrite = mRead;  //should be one ahead.
        } else {
            mWrite = (mWrite+1)%mCapacity;
            mSize++;
        }
    };
    
    //rolls over.
    Type& pop_front(void) {  //because I'm usually going to want to return vectors without a copy.  For instance, this is what is predominantely being called in Keda code
        if (mSize > 0) {
            Type& retVal = mInternalBuffer[mRead];  //copy assignment.
            mRead = (mRead+1)%mCapacity;      //Advance the rd ptr.
            mSize--;                  //this will always be true;
            return retVal;
        } else {
            assert(false);
            mInternalBuffer[0] = mNullVal; //Buffer shouldn't hold anything valid so just overwrite with trash.
            return mInternalBuffer[0];
        }
    };
    
    /*
     const Type& pop_front(void) const {  //because I'm usually going to want to return vectors  (Not sure I've tested this one well.)
     if (mSize > 0) {
     const Type retVal = std::move(mInternalBuffer[mRead]);
     mRead = (mRead+1)%mCapacity;      //Advance the rd ptr.
     mSize--;                  //this will always be true;
     return retVal;
     } else {
     return mNullVal;
     }
     };
     */
    
    Type& front(void) {
        return mInternalBuffer[mRead];
    }

    Type& back(void) {
        if (mWrite == 0) {
            return mInternalBuffer[mCapacity-1];
        } else {
            return mInternalBuffer[mWrite-1];
        }
    }
    

    
    Type& operator[](const int pos){
        long shiftedPos = ((mRead+pos) % mCapacity);
        return mInternalBuffer[shiftedPos];
    };
    
    const Type operator[](const int pos) const {
        int shiftedPos = ((mRead+pos) % mCapacity);
        return mInternalBuffer[shiftedPos];
    };
    
    //same as above but less elegant.
    //Not particularly efficient but...  //Set el doesn't impact size or check any sort of validity.
    void setEl(const int inSampleNum, const Type inSample){
        int shiftedPos = ((mRead+inSampleNum) % mCapacity);
        mInternalBuffer[shiftedPos] = inSample;
    }
    
    void addBlock(const typename std::vector<Type>::iterator segStart,  const typename std::vector<Type>::iterator segEnd){
        int numToAdd = segEnd-segStart;

        assert(numToAdd < mCapacity);

        int tillWrap = mCapacity - mWrite;
        if (tillWrap <= numToAdd) {
            std::copy(segStart, segEnd, mInternalBuffer.begin() + mWrite);
            mWrite += numToAdd;
        } else {
            std::copy(segStart, segStart+tillWrap, mInternalBuffer.begin() + mWrite);
            std::copy(segStart + tillWrap, segEnd, mInternalBuffer.begin());
            mWrite = numToAdd - tillWrap;
        }
        if ((mCapacity - mSize) < numToAdd){ //an overwrite has occurred, update my read pointer.
            mOverwriteOccurred_Flag = true;
            mRead = mWrite;  //should be one ahead.
            mSize = mCapacity;
        } else { //update size
            mSize += numToAdd;
        }
        
    }
    
    //I SHOULD HAVE A READBOCK BUT I DON'T NEED IT YET
    void addBlock(const std::vector<Type>& block_to_add){
        assert(block_to_add.size() <= mCapacity);
        long numToAdd = block_to_add.size();
        long tillWrap = mCapacity - mWrite;
        if (tillWrap > numToAdd) {
            std::copy(block_to_add.begin(), block_to_add.end(), mInternalBuffer.begin() + mWrite);
            mWrite += numToAdd;
        } else {
            std::copy(block_to_add.begin(), block_to_add.begin()+tillWrap, mInternalBuffer.begin() + mWrite);
            std::copy(block_to_add.begin() + tillWrap, block_to_add.end(), mInternalBuffer.begin());
            mWrite = numToAdd - tillWrap;
        }
        if ((mCapacity - mSize) < numToAdd){ //an overwrite has occurred, update my read pointer.
            mOverwriteOccurred_Flag = true;
            mRead = mWrite;  //see comment above, wr points to next to write to (i.e. future) rd points to NOW
            mSize = mCapacity;
        } else { //update size
            mSize += numToAdd;
        }
    }

    //this does NOT move read pointer.  Read block would do that.  Rather this is meant to be similar to std::vector.data()
    void getData(std::vector<Type>& outData){
        
        outData.resize(mSize);

        long tillWrap = mCapacity - mRead;
        if (mSize == mCapacity){
            std::copy(mInternalBuffer.begin()+mRead, mInternalBuffer.end(), outData.begin());
            std::copy(mInternalBuffer.begin(), mInternalBuffer.begin()+mRead, outData.begin()+tillWrap);
        } else {
            if (tillWrap > mSize) {
                std::copy(mInternalBuffer.begin()+mRead, mInternalBuffer.begin()+mSize, outData.begin());
            } else {
                std::copy(mInternalBuffer.begin()+mRead, mInternalBuffer.end(), outData.begin());
                std::copy(mInternalBuffer.begin(), mInternalBuffer.begin()+(tillWrap-mSize), outData.begin()+tillWrap);
            }
        }
    }

    //this does NOT move read pointer.  Read block would do that.  Rather this is meant to be a similar to std::vector.data(), but only returning a smaller block at the begining that is matched to the outData size.
    //(haven't tested speediness yet)
    void getBlock(std::vector<Type>& outData){
        
        long mOutSize = outData.size();
        long tillWrap = mCapacity - mRead;
        if (mOutSize > mCapacity){  //Do everything as normal but just filling in the beginning of outData
            if (mSize == mCapacity){    //full
                std::copy(mInternalBuffer.begin()+mRead, mInternalBuffer.end(), outData.begin());
                std::copy(mInternalBuffer.begin(), mInternalBuffer.begin()+mRead, outData.begin()+tillWrap);
            } else {
                if (tillWrap > mSize) {
                    std::copy(mInternalBuffer.begin()+mRead, mInternalBuffer.begin()+mSize, outData.begin());
                } else {
                    std::copy(mInternalBuffer.begin()+mRead, mInternalBuffer.end(), outData.begin());
                    std::copy(mInternalBuffer.begin(), mInternalBuffer.begin()+(tillWrap-mSize), outData.begin()+tillWrap);
                }
            }
        } else {    //outData is smaller
            if (mSize == mCapacity){    //full
                if (mOutSize < tillWrap) {
                    std::copy(mInternalBuffer.begin()+mRead, mInternalBuffer.begin() + mRead + mOutSize, outData.begin());
                } else {
                    std::copy(mInternalBuffer.begin()+mRead, mInternalBuffer.end(), outData.begin());
                    std::copy(mInternalBuffer.begin(), mInternalBuffer.begin()+ (mOutSize-tillWrap), outData.begin()+tillWrap);
                }
            } else { //not full
                if (tillWrap > mSize) {
                    if (mOutSize > mSize) std::copy(mInternalBuffer.begin()+mRead, mInternalBuffer.begin()+mSize, outData.begin());
                    if (mOutSize < mSize) std::copy(mInternalBuffer.begin()+mRead, mInternalBuffer.begin()+mOutSize, outData.begin());
                } else {
                    if (mOutSize < tillWrap) {
                        std::copy(mInternalBuffer.begin()+mRead, mInternalBuffer.begin() + mOutSize, outData.begin());
                    } else {
                        std::copy(mInternalBuffer.begin()+mRead, mInternalBuffer.end(), outData.begin());
                        std::copy(mInternalBuffer.begin(), mInternalBuffer.begin()+ (mOutSize-tillWrap), outData.begin()+tillWrap);
                    }
                }
            }
        }
    }

    
    const Type last(void) { //read only version
        if (mWrite == 0) {
            return mInternalBuffer[mCapacity-1];
        }
        else {
            return mInternalBuffer[mWrite-1];
        }
    };
    
    const long spaceFree(void){
        return (mCapacity - mSize);
    };
    
    const long size(void){
        return (mSize);
    };

    const bool hasOverwritten(void){
        bool returnVal = mOverwriteOccurred_Flag;
        mOverwriteOccurred_Flag = false;
        return returnVal;
    }
    
    void clear(void){
        mInternalBuffer.assign(mCapacity, mNullVal);  //guess I can't  have an array of standard vectors?
        mRead = 0;
        mWrite = 0;
        mSize = 0;
        mOverwriteOccurred_Flag = false;
    }
    
    bool isFull(void){ return (mSize == mCapacity); };
    bool isEmpty(void) { return (mSize == 0); };
    
private:
    
    std::vector<Type> mInternalBuffer;
    bool mOverwriteOccurred_Flag;
    
    const long    mCapacity;
    const Type   mNullVal;
    
    unsigned long mSize;
    unsigned long mRead;       //This is the rd ptr
    unsigned long mWrite;      //This is the wr ptr
};

#endif
