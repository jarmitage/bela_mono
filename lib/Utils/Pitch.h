/*
 * Pitch utils
 * https://github.com/jarmitage/resonators
 * 
 * Port of [resonators~] for Bela:
 * https://github.com/CNMAT/CNMAT-Externs/blob/6f0208d3a1/src/resonators~/resonators~.c
 */

// Related issue: https://github.com/BelaPlatform/Bela/issues/554

#ifndef Pitch_H_
#define Pitch_H_

#include <string>
#include <vector>
#include <fstream>
#include <map>

class Pitch
{
public:
  Pitch(){};
  ~Pitch(){};

  float midiToFreq (int _note) { return 27.5 * pow(2, (((float)_note - 21)/12)); }
  int freqToMidi (float _freq) { return (12/log(2)) * log(_freq/27.5) + 21; }
  int noteNameToMidi (std::string noteName) {
      auto findNote = midiNoteNames.find(noteName);
      if (findNote != midiNoteNames.end()){
          return findNote->second;
      }
      else {
          rt_printf("[PitchUtils] Error: note not found \'%ls\'\n", noteName.c_str());
          return -1;
      }
  }
  float noteNameToFreq (std::string noteName) { return midiToFreq(noteNameToMidi(noteName)); }
  std::string midiToNoteName(int _note) {
      std::map<std::string, int>::const_iterator it;

      for (it = midiNoteNames.begin(); it != midiNoteNames.end(); ++it)
          if (it->second == _note) return it->first;

      return "[PitchUtils] Error: note out of range?";
  }
  std::string freqToNoteName(float _freq) { return midiToNoteName(freqToMidi(_freq)); }
  
private:
  std::map<std::string, int> midiNoteNames = {                                                                 {"a0",21}, {"as0",22}, {"b0",23},
    {"c1",24}, {"cs1",25}, {"d1",26}, {"ds1",27}, {"e1",28}, {"f1", 29}, {"fs1",30}, {"g1", 31}, {"gs1",32}, {"a1",33}, {"as1",34}, {"b1",35},
    {"c2",36}, {"cs2",37}, {"d2",38}, {"ds2",39}, {"e2",40}, {"f2", 41}, {"fs2",42}, {"g2", 43}, {"gs2",44}, {"a2",45}, {"as2",46}, {"b2",47},
    {"c3",48}, {"cs3",49}, {"d3",50}, {"ds3",51}, {"e3",52}, {"f3", 53}, {"fs3",54}, {"g3", 55}, {"gs3",56}, {"a3",57}, {"as3",58}, {"b3",59},
    {"c4",60}, {"cs4",61}, {"d4",62}, {"ds4",63}, {"e4",64}, {"f4", 65}, {"fs4",66}, {"g4", 67}, {"gs4",68}, {"a4",69}, {"as4",70}, {"b4",71},
    {"c5",72}, {"cs5",73}, {"d5",74}, {"ds5",75}, {"e5",76}, {"f5", 77}, {"fs5",78}, {"g5", 79}, {"gs5",80}, {"a5",81}, {"as5",82}, {"b5",83},
    {"c6",84}, {"cs6",85}, {"d6",86}, {"ds6",87}, {"e6",88}, {"f6", 89}, {"fs6",90}, {"g6", 91}, {"gs6",92}, {"a6",93}, {"as6",94}, {"b6",95},
    {"c7",96}, {"cs7",97}, {"d7",98}, {"ds7",99}, {"e7",100},{"f7", 101},{"fs7",102},{"g7", 103},{"gs7",104},{"a7",105},{"as7",106},{"b7",107},
    {"c8",108},{"cs8",109},{"d8",110},{"ds8",111},{"e8",112},{"f8", 113},{"fs8",114},{"g8", 115},{"gs8",116},{"a8",117},{"as8",118},{"b8",119},
    {"c9",120},{"cs9",121},{"d9",122},{"e9", 123},{"f9",124},{"fs9",125},{"g9", 126},{"gs9",127}
  };

};

#endif /* Pitch_H_ */
