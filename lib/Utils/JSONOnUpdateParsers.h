// onUpdate parser functions for Effects libraries

// TODO: ifdefs for different functions

#include <iostream>

class JSONOnUpdateParsers
{
public:
  JSONOnUpdateParsers(){}
  ~JSONOnUpdateParsers(){}

  /*
   **************************************************
   * FILTERS
   **************************************************
   */

  // EQ
  void onUpdateEQ(BiquadEQ &_eq, JSONValue *args){
    // JSONObject argsObj = args->AsObject();
    // std::vector<BiquadParams> tmp_params;
    // std::vector<int> types = (int) argsObj[L"types"]->AsArray();
    // float  Fc   = (float)  argsObj[L"Fcs"]->AsArray();
    // float  Q    = (float)  argsObj[L"Qs"]->AsArray();
    // double gain = (double) argsObj[L"gains"]->AsArray();
    // _eq.updateEQ(tmp_params);
  }

  void onUpdateBiquadEQ(BiquadEQ &_eq, JSONValue *args){
    JSONObject argsObj = args->AsObject();
    int    i    = (int)    argsObj[L"index"]->AsNumber();
    int    type = (int)    argsObj[L"type"]->AsNumber();
    float  Fc   = (float)  argsObj[L"Fc"]->AsNumber();
    float  Q    = (float)  argsObj[L"Q"]->AsNumber();
    double gain = (double) argsObj[L"gain"]->AsNumber();
    _eq.updateBiquad(i, type, Fc, Q, gain);
  }

  // Biquad
  void onUpdateBiquad(Biquad &_bq, JSONValue *args){
    JSONObject argsObj = args->AsObject();
    int    type = (int)    argsObj[L"type"]->AsNumber();
    float  Fc   = (float)  argsObj[L"Fc"]->AsNumber();
    float  Q    = (float)  argsObj[L"Q"]->AsNumber();
    double gain = (double) argsObj[L"gain"]->AsNumber();
    _bq.setType(type);
    _bq.setFc(Fc);
    _bq.setQ(Q);
    _bq.setPeakGain(gain);
    // _bq.updateBiquad(type, Fc, Q, gain);
  }

  // Resonators
  void onUpdateResModel(Resonators &_res, JSONValue *args) {
    JSONObject argsObj = args->AsObject();
    int index = (int) argsObj[L"index"]->AsNumber();
    JSONValue *model = args->Child(L"model");
    _res.setModel(index, model);
  }

  void onUpdateResPitch(Resonators &_res, JSONValue *args) {
    JSONObject argsObj = args->AsObject();
    int index = (int) argsObj[L"index"]->AsNumber();
    std::wstring wspitch = args->Child(L"pitch")->AsString();
    std::string pitch (wspitch.begin(), wspitch.end());
    _res.setPitch(index, pitch);
  }

  void onUpdateResResonators(Resonators &_res, JSONValue *args) {
    // JSONObject argsObj = args->AsObject();
    // int bankIndex = (int) argsObj[L"bankIndex"]->AsNumber();
    // int resIndexes[] = (int) argsObj[L"resIndexes"]->AsArray();
    // JSONValue *params = args->Child(L"params");
    // _res.setResonators(bankIndex, resIndexes, params);
  }

  /*
   **************************************************
   * EFFECTS
   **************************************************
   */

  // COMPRESSOR
  // void onUpdateCompressor(Compressor &_compressor, JSONValue *args){
  //   JSONObject argsObj = args->AsObject();
    
  //   float t  = (float) argsObj[L"threshold"]->AsNumber();
  //   float r  = (float) argsObj[L"ratio"]->AsNumber();
  //   float a  = (float) argsObj[L"attack"]->AsNumber();
  //   float rl = (float) argsObj[L"release"]->AsNumber();
  //   float g  = (float) argsObj[L"gain"]->AsNumber();
  //   float w  = (float) argsObj[L"mix"]->AsNumber();
    
  //   bool k = (bool) argsObj[L"knee"]->AsBool();
  //   bool f = (bool) argsObj[L"transfer"]->AsBool();
  //   bool b = (bool) argsObj[L"bypass"]->AsBool();

  //   _compressor.updateSettings(t, r, a, rl, g, w, k, f, b);
  // }

  // Gates
  void onUpdateGates(std::vector<Gate> &gates, JSONValue *args) {
    JSONObject argsObj = args->AsObject();
    float _threshold = (float) argsObj[L"threshold"]->AsNumber();
    for (int i = 0; i < gates.size(); ++i) {
      if (gates[i].getThreshold() != _threshold)
        gates[i].setThreshold(_threshold);
    }
    json_u.print(args);
    printf("Gate threshold: %2.f\n", gates[0].getThreshold());
  }

  /*
   **************************************************
   * OTHER
   **************************************************
   */

  // IOGain
  void onUpdateGain(std::vector<float> &io_gain, JSONValue *args){
    JSONObject argsObj = args->AsObject();
    float _in  = (float) argsObj[L"in"]->AsNumber();
    float _out = (float) argsObj[L"out"]->AsNumber();
    if (io_gain[0] != _in)  { io_gain[0] = _in;  }
    if (io_gain[1] != _out) { io_gain[1] = _out; }
    // json_u.print(args);
  }

private:
  JSONUtils json_u;
  
};
